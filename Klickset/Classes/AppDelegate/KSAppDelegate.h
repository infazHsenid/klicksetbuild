//
//  KSAppDelegate.h
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSAppDelegate : UIResponder <UIApplicationDelegate>
//Variables
@property (strong, nonatomic) UIWindow *window;
//Methods
/**
 to get a shared instance of KSAppDelegate class
 @return shared instance of KSAppDelegate
 **/
+ (KSAppDelegate *)sharedInstance;
@end

