//
//  FlatButton.h
//  Klickset
//
//  Created by Infaz Ariff on 6/18/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlatButton : UIButton

+ (instancetype)button;
- (void)setup;
@end
