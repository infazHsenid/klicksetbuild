//
//  KSPhotoBrowser.m
//  Klickset
//
//  Created by Infaz Ariff on 7/13/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSPhotoBrowser.h"

@interface KSPhotoBrowser ()

@end

@implementation KSPhotoBrowser

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *shareButton =[self createNavigationBarButtonWithImageName:@"share-icon.png" buttonFrame:CGRectMake(0,0,18,23)withTag:12];
    self.navigationItem.rightBarButtonItem = shareButton;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBarTintColor:RED_THEME_COLOR];
}

- (void)updateNavigation {
    // Title
    self.title = @"Select Videos";
}

- (UIBarButtonItem *)createNavigationBarButtonWithImageName:(NSString *)imagename buttonFrame:(CGRect)buttonFrame withTag:(int)tag{
    UIButton *button =[[UIButton alloc] init];
    [button setBackgroundImage:[UIImage imageNamed:imagename] forState:UIControlStateNormal];
    [button.imageView setContentMode:UIViewContentModeScaleAspectFit];
    button.frame = buttonFrame;
    button.tag = tag;
    [button addTarget:self action:@selector(navigationButtonsAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButton =[[UIBarButtonItem alloc] initWithCustomView:button];
    return barButton;
}

- (void)navigationButtonsAction:(id)sender{
    [self.allShareDelegate allShareButtonTapped];
}

@end
