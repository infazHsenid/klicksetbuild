//
//  UITableView+KSTableView.m
//  Klickset
//
//  Created by Infaz Ariff on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "UITableView+KSTableView.h"

@implementation UITableView (KSTableView)

- (BOOL) allowsHeaderViewsToFloat {
    return NO;
}

- (BOOL) allowsFooterViewsToFloat {
    return NO;
}

@end
