//
//  KSPhotoBrowser.h
//  Klickset
//
//  Created by Infaz Ariff on 7/13/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "MWPhotoBrowser.h"

@protocol AllShareDelegate <NSObject>
- (void)allShareButtonTapped;
@end
@interface KSPhotoBrowser : MWPhotoBrowser
@property (weak,nonatomic) id <AllShareDelegate> allShareDelegate;
@end
