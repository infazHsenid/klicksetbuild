//
//  UITableView+KSTableView.h
//  Klickset
//
//  Created by Infaz Ariff on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (KSTableView)
/**
 Enable or Disable Floating Header view on tableview
 @return YES to enable Floating Header
 **/
- (BOOL) allowsHeaderViewsToFloat;
/**
 Disable or Disable Floating Footer view on tableview
 @return YES to enable Floating Footer
 **/
- (BOOL) allowsFooterViewsToFloat;
@end
