//
//  KSUserAPI.h
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "AFHTTPSessionManager.h"

@interface KSUserAPI : AFHTTPSessionManager
/**
 to get a shared instance of KSUserAPI class
 @return shared instance of KSUserAPI
 **/
+ (instancetype)sharedUserAPI;
/**
 Check network status
 **/
- (BOOL)checkNetworkStatus:(void (^)(NSError *error))failureBlock;
/**
 Get Videos List
 @param lastID ID of last returned video
 @param recordsPerPage pass the number of records to be returned
 **/
- (void)getVideoForLastID:(NSString *)lastID andRecordsPerPage:(NSString *)recordsPerPage success:(void (^)(NSMutableArray *array))success failure:(void (^)(NSError *error))failure;
/**
 Get Videos List
 @param videoPath path of the video
 @param caption caption to be sent for the uploading video
 **/
- (void)postNewVideo:(NSString *)videoPath passingCaption:(NSString *)caption success:(void (^)(NSMutableArray *videoArr))success failure:(void (^)(NSMutableArray *errorArr))failure;
@end
