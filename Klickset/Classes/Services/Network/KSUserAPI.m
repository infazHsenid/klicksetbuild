 //
//  KSUserAPI.m
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSUserAPI.h"
#import <AFNetworkReachabilityManager.h>
#import "KSVideo.h"
#import <AFHTTPRequestOperation.h>
#import <AFHTTPRequestOperationManager.h>

@interface KSUserAPI()
@property __block BOOL isNetworkConnected;
@end

@implementation KSUserAPI

+ (instancetype)sharedUserAPI {
    static KSUserAPI *_sharedUserAPI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedUserAPI = [[KSUserAPI alloc] initWithBaseURL:[NSURL URLWithString:KlickSetBaseURL]];
        _sharedUserAPI.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        _sharedUserAPI.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        NSUserDefaults *userInfo = [NSUserDefaults standardUserDefaults];
        [userInfo setObject:@"289" forKey:STORED_USER_ID];
    });
    return _sharedUserAPI;
}

- (BOOL)checkNetworkStatus:(void (^)(NSError *error))failureBlock {
    if (([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable) && !_isNetworkConnected) {
        NSError *error = [NSError errorWithDomain:KS_NETWOTK_NOT_REACHABLE_ERROR code:KS_NETWOTK_NOT_REACHABLE_ERROR_CODE userInfo:@{KS_PRINTABLE_ERROR_DESC : NSLocalizedString(KS_PRINTABLE_ERROR_DESC, "no internet error")}];
        if (failureBlock) {
            failureBlock(error);
        }
        return NO;
    }
    return YES;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    __weak typeof(self) weakSelf = self;
    if (self) {
        self.isNetworkConnected = !([AFNetworkReachabilityManager sharedManager].networkReachabilityStatus == AFNetworkReachabilityStatusNotReachable);
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            
            __strong typeof(weakSelf) strongSelf = weakSelf;
            if (strongSelf) {
                BOOL previousStatus = strongSelf.isNetworkConnected;
                
                strongSelf.isNetworkConnected = !(status == AFNetworkReachabilityStatusNotReachable || status == AFNetworkReachabilityStatusUnknown);
                
                if (previousStatus != strongSelf.isNetworkConnected) {//only send if change
                    if (!strongSelf.isNetworkConnected) {
                        //Show alert message
                    }
                }
            }
        }];
    }
    return self;
}

- (void)getVideoForLastID:(NSString *)lastID andRecordsPerPage:(NSString *)recordsPerPage success:(void (^)(NSMutableArray *array))success failure:(void (^)(NSError *error))failure{
    if (![self checkNetworkStatus:failure]) return;
    NSString *path = [NSString stringWithFormat:@"testgetVideo.php"];
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:STORED_USER_ID],@"user_id",@10,@"lasted",@20,@"records_per_page", nil];
    [self GET:path parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if([[responseObject objectForKey:@"error_code"] intValue]==0){
            NSMutableArray *videoArray = [NSMutableArray array];
            NSDictionary *responseDict = [responseObject objectForKey:@"result"];
            for (NSDictionary *videoDict in responseDict) {
                KSVideo *video = [[KSVideo alloc]init];
                video.videoID = videoDict[@"video_id"];
                video.videoPath = [videoDict objectForKey:@"video_path"];
                video.videoThumbPath = videoDict[@"video_thumb_path"];
                video.videoLength = videoDict[@"videolength"];
                video.videoDescription = videoDict[@"description"];
                video.commentsCount = videoDict[@"count_comment"];
                video.likesCount = videoDict[@"count_like"];
                video.user = [[KSUser alloc]init];
                video.user.userId = videoDict[@"user_id"];
                video.user.username = videoDict[@"username"];
                video.user.image = [[KSImage alloc]init];
                video.user.image.photo_thumb = videoDict[@"image_path"];
                video.isLiked = [videoDict[@"islike"] boolValue];
                video.isReported = [videoDict[@"isreport"] boolValue];
                video.createdDate = videoDict[@"video_created"];
                [videoArray addObject:video];
            }
            if (success) {
                success(videoArray);
            }
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (void)postNewVideo:(NSString *)videoPath passingCaption:(NSString *)caption success:(void (^)(NSMutableArray *videoArr))success failure:(void (^)(NSMutableArray *errorArr))failure {
    
    
    NSString *path = [NSString stringWithFormat:@"http://www.klickset.com/klickset/web-services/build_1.5/test-upload_video.php"];
    UIImage *thumbImage = [KSCommonUtils generateThumbImage:videoPath];
    
    NSString* videoFileName = [[videoPath lastPathComponent] stringByDeletingPathExtension];
    NSData *imagedata = UIImagePNGRepresentation(thumbImage);

////    [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
////                                        long long totalBytesWritten,
////                                        long long totalBytesExpectedToWrite) {
////        double percentDone = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
////        NSLog(@"progress updated(percentDone) : %f", percentDone);
////    }];
//
//    
//    
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:videoUpRequest];
//    operation.responseSerializer = [AFJSONResponseSerializer serializer];
//    [AFHTTPSessionManager manager].securityPolicy.allowInvalidCertificates = YES;
//    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//        float progress = totalBytesWritten / (float)totalBytesExpectedToWrite;
//        NSLog(@"%f",progress);
//    }];
//
//
    
    
    
    
//    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:@"http://www.klickset.com/klickset/web-services/build_1.5/test-upload_video.php" parameters:@{@"description":caption} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFileURL:[NSURL fileURLWithPath:videoPath] name:@"video" fileName:@"filename.mp4" mimeType:@"video/mp4" error:nil];
//    } error:nil];
//    
//    NSProgress *progress = nil;
//    
//    NSURLSessionUploadTask *uploadTask = [self uploadTaskWithStreamedRequest:request progress:&progress completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
//        if (!error) {
//            NSLog(@"%@", progress);
//            NSLog(@"%@ %@", response, responseObject);
//        } else {
//            NSLog(@"Error: %@", error);
//        }
//    }];
//    
//    NSLog(@"%@", progress);
//    [uploadTask resume];
    
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:path parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:videoPath] name:@"video" fileName:[NSString stringWithFormat:@"%@.m4v",videoFileName] mimeType:@"video/m4v" error:nil];
        [formData appendPartWithFileData:imagedata name:@"videothumb" fileName:[NSString stringWithFormat:@"%@.png",videoFileName] mimeType:@"image/png"];
    } error:nil];
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperationManager manager] HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        // Do stuff here
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Catch error
    }];
    requestOperation.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [requestOperation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        double percentDone = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
        
         NSLog(@"%f",percentDone);
    }];
    [requestOperation start];
}

//Work in progress
- (BOOL)verifyMobileNumber:(NSString *)mobileNumber andCountryCode:(NSString *)countryCode{
    return YES;
}

//Work in progress
- (KSUser *)getProfileDetailsforUserID:(NSString *)userID{
    KSUser *user = [[KSUser alloc]init];
    user.username = @"Sarah Green";
    user.countryName = @"Spain";
    user.followersCount = @"243";
    user.followingcount = @"124";
    user.postsCounts = @"86";
    user.bio = @"I am sarah from Spain,I can sing well and love football";
    user.favouritesCount = @"3";
    return user;
}

@end
