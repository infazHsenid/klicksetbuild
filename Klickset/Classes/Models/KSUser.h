//
//  KSUser.h
//  Klickset
//
//  Created by Infaz Ariff on 6/15/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSImage.h"

@interface KSUser : NSObject
@property (copy, nonatomic)NSString *userId;
@property (copy, nonatomic)NSString *name;
@property (copy, nonatomic)NSString *username;
@property (copy, nonatomic)NSString *bio;
@property (copy, nonatomic)NSString *countryName;
@property (copy, nonatomic)NSString *followersCount;
@property (copy, nonatomic)NSString *followingcount;
@property (copy, nonatomic)NSString *postsCounts;
@property (copy, nonatomic)NSString *favouritesCount;
@property (strong, nonatomic) KSImage *image;
@end
