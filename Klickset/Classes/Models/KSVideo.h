//
//  KSVideo.h
//  Klickset
//
//  Created by Infaz Ariff on 6/15/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSUser.h"

@interface KSVideo : NSObject
@property (copy, nonatomic)NSString *commentsCount;
@property (copy, nonatomic)NSString *likesCount;
@property (copy, nonatomic)NSString *country;
@property (copy, nonatomic)NSString *videoDescription;
@property (copy, nonatomic)NSString *videoID;
@property (copy, nonatomic)NSString *user_visibility;
@property (copy, nonatomic)NSString *videoPath;
@property (copy, nonatomic)NSString *videoThumbPath;
@property (copy, nonatomic)NSString *videoLength;
@property BOOL isReported;
@property BOOL isLiked;
@property (strong, nonatomic)KSUser *user;
@property (strong, nonatomic)NSString *createdDate;
@end
