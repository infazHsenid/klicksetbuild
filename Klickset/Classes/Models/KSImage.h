//
//  KSImage.h
//  Klickset
//
//  Created by Infaz Ariff on 6/15/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KSImage : NSObject
@property (copy, nonatomic)NSString *photo_thumb;
@property (copy, nonatomic)NSString *profile_photo_full;
@end
