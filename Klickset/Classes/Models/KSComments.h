//
//  KSComments.h
//  Klickset
//
//  Created by Pramuka Dias on 7/7/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface KSComments : NSObject
@property (nonatomic, strong) NSString *profileImageName;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *commentText;
@end
