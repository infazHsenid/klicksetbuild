//
//  KSMoreViewController.m
//  Klickset
//
//  Created by Infaz Ariff on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSMoreViewController.h"
#import "KSFavouriteVideoCell.h"
#import "UITableView+KSTableView.h"
#import "KSUserAPI.h"
#import "KSVideo.h"
#import <UIImageView+AFNetworking.h>
#import "KSCommonUtils.h"

@interface KSMoreViewController ()
@property (weak, nonatomic) IBOutlet UITableView *favouriteTableView;
@property (strong, nonatomic) NSMutableArray *videoArray;
@end

@implementation KSMoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_favouriteTableView allowsHeaderViewsToFloat];
    [self getVideos];
}

-(void)getVideos{
    [self showLoading];
    [[KSUserAPI sharedUserAPI]getVideoForLastID:@"20" andRecordsPerPage:@"6" success:^(NSMutableArray *array) {
        _videoArray = array;
        _videoArray = [KSCommonUtils splitArray:_videoArray componentsPerSegment:3];
        [UIView transitionWithView:self.favouriteTableView
                          duration:0.35f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^(void)
         {[self.favouriteTableView reloadData];
             [self stopLoading];
         }completion: nil];
    } failure:^(NSError *error) {
        [self stopLoading];
        [self showAlertViewWithTitle:nil andSubTitle:@"Network Not Found" andCloseButtonTitle:@"Ok" forType:alert_Error];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configureNavigationwithTitle:@""];
    [self addBarButtonsToNavigationBar];
}

#pragma mark TabelView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 50.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        return ((IS_IPAD) ? 25.0f:(IS_IPhone6Plus) ? 21.0f:(IS_IPhone6) ? 18.0f:15.0f);
    }
    return ((IS_IPAD) ? 124.0f:(IS_IPhone6Plus) ? 104.0f:(IS_IPhone6) ? 94.0f:84.0f);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(_videoArray.count > 0){
        static NSString *favCellHeaderIdentifier = @"favouriteCellHeader";
        KSFavouriteVideoCell *header = [_favouriteTableView dequeueReusableCellWithIdentifier:favCellHeaderIdentifier];
        if (!header) {
            NSArray *nibArr = [[NSBundle mainBundle]loadNibNamed:@"KSFavouriteVideoCell" owner:self options:nil];
            header = [nibArr objectAtIndex:0];
        }
        if (section == 0) {
            header.headerLabel.text = @"Favourite Video";
        }else{
            header.headerLabel.text = @"Recent Video";
        }
        header.selectionStyle=UITableViewCellSelectionStyleNone;
        return header;
    }else{
        return nil;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return (_videoArray.count > 0)?2:0;;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (_videoArray.count > 0)?4:0;
}

#pragma mark TabelView DataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *profileCellHeaderIdentifier = @"favouriteCell";
    static NSString *moreCellHeaderIdentifier = @"moreCell";
    KSFavouriteVideoCell *favCell = [_favouriteTableView dequeueReusableCellWithIdentifier:profileCellHeaderIdentifier];
    KSFavouriteVideoCell *moreCell = [_favouriteTableView dequeueReusableCellWithIdentifier:moreCellHeaderIdentifier];
    NSArray *nibArr = [[NSBundle mainBundle]loadNibNamed:@"KSFavouriteVideoCell" owner:self options:nil];
    if (indexPath.row == 3) {
        if (!moreCell) {
            moreCell = [nibArr objectAtIndex:2];
        }
        favCell.selectionStyle=UITableViewCellSelectionStyleNone;
        return moreCell;
    }else{
        if (!favCell) {
            favCell = [nibArr objectAtIndex:1];
        }
        KSVideo *videos = [[_videoArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        [favCell.userProfileImage setImageWithURL:[NSURL URLWithString:videos.videoThumbPath] placeholderImage:VIDEO_PLACEHOLDER];
        favCell.userProfileImage.layer.masksToBounds = YES;
        favCell.uploadedUserLabel.text = [NSString stringWithFormat:@"By %@",videos.user.username];
        [favCell.userProfileButton  addTarget:self action:@selector(getSelectedVideo:) forControlEvents:UIControlEventTouchUpInside];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigateToUserProfile:)];
        [favCell.uploadedUserLabel addGestureRecognizer:tapGesture];
        favCell.videoCaptionLabel.text = [KSCommonUtils setTextValidationWithDash:videos.videoDescription];
        favCell.numberOfViewsLabel.text=@"100 Views";
        favCell.selectionStyle=UITableViewCellSelectionStyleNone;
        return favCell;
    }
}

-(void)addBarButtonsToNavigationBar{
    self.tabBarController.navigationItem.leftBarButtonItems=nil;
    self.tabBarController.navigationItem.rightBarButtonItem=nil;
    UIButton *button =[[UIButton alloc] init];
    [button setBackgroundImage:[UIImage imageNamed:@"Search-icon.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(showSearchView) forControlEvents:UIControlEventTouchUpInside];
    [button.imageView setContentMode:UIViewContentModeScaleAspectFit];
    button.frame = CGRectMake(0,0,23,23);
    UIBarButtonItem *searchButton =[[UIBarButtonItem alloc] initWithCustomView:button];
    self.tabBarController.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchButton,nil];
}

// navigate to user profile view
- (void)navigateToUserProfile:(id)sender {
    NSIndexPath *indexPath =[self.favouriteTableView indexPathForRowAtPoint:[sender locationInView:self.favouriteTableView]];
    KSVideo *videos = [[_videoArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    [self navigateToUserProfileViewWith:videos.user];
}

-(void)showSearchView{
    [self navigateToSearchView];
}

-(void)getSelectedVideo:(id)sender{
    NSIndexPath *indexPath = [self.favouriteTableView indexPathForRowAtPoint:[sender convertPoint:CGPointZero toView:self.favouriteTableView]];
    KSVideo *videos = [[_videoArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    NSLog(@"%@",videos.user.username);
}

@end
