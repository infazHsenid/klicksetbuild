//
//  KSMoreViewController.h
//  Klickset
//
//  Created by Infaz Ariff on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSAbstractUIViewController.h"

@interface KSMoreViewController : KSAbstractUIViewController

@end
