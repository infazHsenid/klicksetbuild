//
//  KSSettingsViewController.m
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSPeopleViewController.h"
#import "KSPeoplesTableViewCell.h"
#import "UIViewController+JCAdditionsPage.h"

@interface KSPeopleViewController () <UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) UISegmentedControl *segmentController;
@property (weak, nonatomic) IBOutlet UITableView *peoplesTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSArray *sectionTitleArray;
@property (strong, nonatomic) NSDictionary *peoplesDict;
@property (strong, nonatomic) NSArray *lettersArray;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@end

@implementation KSPeopleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!_segmentController) {
        _segmentController = [[UISegmentedControl alloc]initWithItems:[NSArray arrayWithObjects:@"All Klickset",@"Friends",@"All Contacts", nil]];
        [_segmentController addTarget:self action:@selector(segmentControlAction:) forControlEvents: UIControlEventValueChanged];
        _segmentController.selectedSegmentIndex = 0;
        NSDictionary *normalAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIColor whiteColor], NSForegroundColorAttributeName,
                                          nil];
        [_segmentController setTitleTextAttributes:normalAttributes forState:UIControlStateNormal];
        [_segmentController setTitleTextAttributes:normalAttributes forState:UIControlStateSelected];
        _segmentController.tintColor = RED_SEGEMENT_TINT_COLOR;
    }
//    [self enabledPullToRefreshAndLoadMore:self.peoplesTableView];
    [self generateTableDataSource];
    _searchBar.userInteractionEnabled=NO;
    [self setupTableViewRefresh:self.peoplesTableView];
}

- (void)loadDatas{
//    [_peoplesTableView reloadData];
}

- (void)segmentControlAction:(UISegmentedControl *)segment{
    [self generateTableDataSource];
    [_peoplesTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar.topItem setTitleView:_segmentController];
    self.tabBarController.navigationItem.leftBarButtonItems=nil;
    self.tabBarController.navigationItem.rightBarButtonItems=nil;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar.topItem setTitleView:nil];
}
-(void)generateTableDataSource{
    _peoplesDict = @{@"A" : @[@"Ann Lawondski", @"Amal Khan", @"Anastasia Peters", @"Aron Fernando"],
                     @"B" : @[@"Bethony Thomas", @"Brendon Great", @"Bella"],
                     @"C" : @[@"Carol", @"Caroline", @"Claire"],
                     @"D" : @[@"Diana", @"Dorothy"],
                     @"E" : @[@"Elizabeth"],
                     @"G" : @[@"Gabrielle", @"Grace"],
                     @"H" : @[@"Hannah"],
                     @"K" : @[@"Karen", @"Katherine"],
                     @"L" : @[@"Lauren", @"Lisa"],
                     @"M" : @[@"Madeleine", @"Mary",@"Melanie",@"Michelle"],
                     @"R" : @[@"Rachel",@"Rebecca",@"Rose"],
                     @"S" : @[@"Sarah",@"Sophie"],
                     @"T" : @[@"Tracey"],
                     @"W" : @[@"Wendy", @"Warren", @"William"]};
    _sectionTitleArray = [[_peoplesDict allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    _lettersArray = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z",@"#"];
    [_peoplesTableView setSectionIndexColor:RED_THEME_COLOR];
}

#pragma mark TabelView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _sectionTitleArray.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [_sectionTitleArray objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[_peoplesDict objectForKey:[_sectionTitleArray objectAtIndex:section]]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *peoplesCellIdentifier = @"peoplesIdentifier";
    KSPeoplesTableViewCell *cell = [_peoplesTableView dequeueReusableCellWithIdentifier:peoplesCellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"KSPeoplesTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    cell.friendNameLabel.text = [[_peoplesDict objectForKey:[_sectionTitleArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    return _lettersArray;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    return [_sectionTitleArray indexOfObject:title];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self navigateToUserProfileViewWith:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark TabelView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  ((IS_IPAD) ? 90.0f:(IS_IPhone6Plus) ? 80.0f:(IS_IPhone6) ? 70.0f:62.0f);
}

@end
