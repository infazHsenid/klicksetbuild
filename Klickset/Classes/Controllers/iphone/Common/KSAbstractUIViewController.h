//
//  AbstractUIViewController.h
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSUserAPIConstants.h"
#import <SCLAlertView.h>
#import "KSUser.h"
#import "KSLoadingIndicator.h"

/**
 Alert Types for show Alert View
 **/
typedef enum {
    alert_Success,
    alert_Error,
    alert_Warning,
    alert_Notice
} ALERTTYPE;

@interface KSAbstractUIViewController : UIViewController
@property (strong ,nonatomic) SCLAlertView *waitingAlert;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
/**
 show alert views
 @param title title of the alert
 @param subTitle subTitle of the alert
 @param closeButtonTitle name of the button of close button
 @alertType type of the alert (ALERTTYPE)
 */
- (void)showAlertViewWithTitle:(NSString *)title andSubTitle:(NSString *)subTitle andCloseButtonTitle:(NSString *)closeButtonTitle forType:(ALERTTYPE)alertType;
/**
 show alert views
 @param title title of the navigation bar
 */
- (void)configureNavigationwithTitle:(NSString *)title;
- (void)showAlertViewForWaitingWithTitle:(NSString *)title andSubTitle:(NSString *)subTitle;
- (void)hideWaitingAlert;
/**
 Navigate to user profile View
 @param user userObject of the user
 **/
- (void)navigateToUserProfileViewWith:(KSUser *)user;
/**
 Navigate to comments View
 @param user userObject of the user
 **/
- (void)navigateToUserCommentsViewWith:(KSUser *)user isFromButton:(BOOL)isFromButton;
/**
 Show loading indicator
 **/
- (void)showLoading;
/**
 Stop and remove loading indicator
 **/
- (void)stopLoading;
/**
 Add a refresh controller to table view
 **/
-(void)setupTableViewRefresh:(UITableView *)tableView;
/**
 Navigate to search View
 **/
- (void)navigateToSearchView;
@end
