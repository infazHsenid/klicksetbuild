//
//  AbstractUIViewController.m
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSAbstractUIViewController.h"
#import "KSProfileViewController.h"
#import <SCLAlertView.h>
#import "KSUser.h"
#import "KSVideoCommentViewController.h"
#import "KSSearchViewController.h"
@interface KSAbstractUIViewController ()
@end

@implementation KSAbstractUIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlertViewWithTitle:(NSString *)title andSubTitle:(NSString *)subTitle andCloseButtonTitle:(NSString *)closeButtonTitle forType:(ALERTTYPE)alertType;
{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    switch (alertType) {
        case alert_Success:
            break;
        case alert_Error:
            [alert showError:self title:title subTitle:subTitle closeButtonTitle:closeButtonTitle duration:0.0f];
            break;
        case alert_Warning:
            break;
        default:
            break;
    }
}

-(void)configureNavigationwithTitle:(NSString *)title{
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setHidden:NO];
    self.tabBarController.navigationItem.title = title;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
}

- (void)showAlertViewForWaitingWithTitle:(NSString *)title andSubTitle:(NSString *)subTitle{
    _waitingAlert = [[SCLAlertView alloc] init];
    _waitingAlert.customViewColor = RED_THEME_COLOR;
    [_waitingAlert showWaiting:self title:title subTitle:nil closeButtonTitle:nil duration:0.0f];
}

- (void)hideWaitingAlert{
    [_waitingAlert hideView];
}

- (void)navigateToUserProfileViewWith:(KSUser *)user{
    KSProfileViewController *profileView = (KSProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
    profileView.selectedUser=user;
    [self.navigationController pushViewController:profileView animated:YES];
}

- (void)navigateToUserCommentsViewWith:(KSUser *)user isFromButton:(BOOL)isFromButton{
    KSVideoCommentViewController *commentView = (KSVideoCommentViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"CommentsView"];
    commentView.isFromCommentButton=isFromButton;
    [self.navigationController pushViewController:commentView animated:YES];
}

- (void)navigateToSearchView{
    KSSearchViewController *searchView = (KSSearchViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SearchView"];
    [self.navigationController pushViewController:searchView animated:YES];
}

- (void)showLoading{
    UIView *loadingView = [[KSLoadingIndicator sharedInstance]startAnimationWithView];
    [self.view addSubview:loadingView];
    [loadingView setTranslatesAutoresizingMaskIntoConstraints:NO];
    UIView *popView = loadingView;
    UIView *sourceView = self.view;
    NSDictionary *views = NSDictionaryOfVariableBindings(popView);
    [sourceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[popView]-0-|" options:0 metrics:nil views:views]];
    [sourceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[popView]-0-|" options:0 metrics:nil views:views]];
}

- (void)stopLoading{
    [[KSLoadingIndicator sharedInstance]stopAnimationOfLoadingView];
}

-(void)setupTableViewRefresh:(UITableView *)tableView{
    _refreshControl = [[UIRefreshControl alloc] init];
    //Adding target to refresh Control object
    [_refreshControl addTarget:self action:@selector(refreshControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    //Adding as subview to tableView
    [tableView addSubview:_refreshControl];
}

- (void)refreshControlValueChanged:(id)sender{
    [self.refreshControl beginRefreshing];
    // simulate loading time
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.refreshControl endRefreshing];
    });
}

@end
