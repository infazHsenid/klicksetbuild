//
//  KSLoadingIndicator.h
//  Klickset
//
//  Created by Infaz Ariff on 6/29/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSLoadingIndicator : UIViewController
/**
 get shared Instance of class
 @return shared Instance
 **/
+(KSLoadingIndicator *)sharedInstance;
/**
 Show Loading view on View
 @return loadingView
 **/
-(UIView *)startAnimationWithView;
/**
 Stop loading view on View
 **/
-(void)stopAnimationOfLoadingView;
@end
