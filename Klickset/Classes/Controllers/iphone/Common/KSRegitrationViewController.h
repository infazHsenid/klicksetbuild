//
//  KSRegitrationViewController.h
//  Klickset
//
//  Created by Pramuka Dias on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSAbstractUIViewController.h"

@interface KSRegitrationViewController : KSAbstractUIViewController

@end
