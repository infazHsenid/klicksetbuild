//
//  KSRegitrationViewController.m
//  Klickset
//
//  Created by Pramuka Dias on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSRegitrationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "CRGradientNavigationBar.h"
#define LEADING_TRAILING_SPACE ((IS_IPAD) ? 90.0f:(IS_IPhone6Plus) ? 45.0f:(IS_IPhone6) ? 43.0f:40.0f)
#define LABEL_TOP_SPACE ((IS_IPAD) ? 50.0f:(IS_IPhone6Plus) ? 83.0f:(IS_IPhone6) ? 73.0f:60.0f)
#define CODE_TOP_SPACE ((IS_IPAD) ? 40.0f:(IS_IPhone6Plus) ? 81.0f:(IS_IPhone6) ? 72.0f:58.0f)
#define NUMBER_TOP_SPACE ((IS_IPAD) ? 45.0f:(IS_IPhone6Plus) ? 45.0f:(IS_IPhone6) ? 40.0f:30.0f)
#define ELEMENTS_HEIGHT ((IS_IPAD) ? 62.0f:(IS_IPhone6Plus) ? 48.0f:(IS_IPhone6) ? 43.0f:38.0f)
#define INPUTVIEW_HEIGHT ((IS_IPAD) ? 62.0f:(IS_IPhone6Plus) ? 50.0f:(IS_IPhone6) ? 46.0f:42.0f)
#define CONTINUE_BUTTON_TOP_SPACE ((IS_IPAD) ? 110.0f:(IS_IPhone6Plus) ? 72.0f:(IS_IPhone6) ? 65.0f:55.0f)
#define CONTINUE_BUTTON_WIDTH ((IS_IPAD) ? 290.0f:(IS_IPhone6Plus) ? 165.0f:(IS_IPhone6) ? 150.0f:123.0f)
#define CONTINUE_BUTTON_HEIGHT ((IS_IPAD) ? 46.0f:(IS_IPhone6Plus) ? 42.0f:(IS_IPhone6) ? 38.0f:31.0f)
#define ELEMENTS_FONT ((IS_IPAD) ? 27.0f:(IS_IPhone6Plus) ? 24.0f:(IS_IPhone6) ? 21.0f:18.0f)
#define INPUTVIEWS_FONT ((IS_IPAD) ? 27.0f:(IS_IPhone6Plus) ? 20.0f:(IS_IPhone6) ? 18.0f:16.0f)
@interface KSRegitrationViewController ()
@property (weak, nonatomic) IBOutlet UILabel *enterPhoneNumberLabel;
@property (weak, nonatomic) IBOutlet UITextField *countyCodeTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
//Constraint
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *elementsHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueButtonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *numberTextFieldTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueButtonTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelTopSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *codeTopSpace;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *inputViewHeight;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *leadingAndTrailingSpace;
//button actions
- (IBAction)continueButtonAction:(id)sender;
@end

@implementation KSRegitrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setHidden:YES];
    [self loadTheViewwithDummyData];
}

- (void)loadTheViewwithDummyData{
    _phoneNumberTextField.text = @"     50486751";
    _countyCodeTextField.text = @"      +001";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configerUIElements{
//    label and code top space
        _labelTopSpace.constant = LABEL_TOP_SPACE;
        _codeTopSpace.constant = CODE_TOP_SPACE;
//    label  height
    for (NSLayoutConstraint *constraint in _elementsHeight) {
        constraint.constant = ELEMENTS_HEIGHT;
    }
    //    code and number elements height
    for (NSLayoutConstraint *constraint in _inputViewHeight) {
        constraint.constant = INPUTVIEW_HEIGHT;
    }
    //    set leading space and trailing space on container view
    for (NSLayoutConstraint *constraint in _leadingAndTrailingSpace) {
        constraint.constant = LEADING_TRAILING_SPACE;
    }
//    number text field top space
    _numberTextFieldTopSpace.constant = NUMBER_TOP_SPACE;
    //   continue button top space,width and height
    _continueButtonTopSpace.constant = CONTINUE_BUTTON_TOP_SPACE;
    _continueButtonWidth.constant = CONTINUE_BUTTON_WIDTH;
    _continueButtonHeight.constant = CONTINUE_BUTTON_HEIGHT;
//    set font and sizes
    [_countyCodeTextField setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:INPUTVIEWS_FONT]];
    [_phoneNumberTextField setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:INPUTVIEWS_FONT]];
    [_continueButton.titleLabel setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:ELEMENTS_FONT]];
    [_enterPhoneNumberLabel setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:ELEMENTS_FONT]];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self configerUIElements];
}

- (IBAction)continueButtonAction:(id)sender {
    
}

@end
