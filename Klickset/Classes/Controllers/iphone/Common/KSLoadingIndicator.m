//
//  KSLoadingIndicator.m
//  Klickset
//
//  Created by Infaz Ariff on 6/29/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSLoadingIndicator.h"

@interface KSLoadingIndicator ()

@end

@implementation KSLoadingIndicator

+(KSLoadingIndicator *)sharedInstance {
    static dispatch_once_t p = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] initWithNibName:@"KSLoadingIndicator" bundle:nil];
    });
    return _sharedObject;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)startAnimationWithView{
    return self.view;
}

- (void)stopAnimationOfLoadingView{
    [self.view removeFromSuperview];
}


@end
