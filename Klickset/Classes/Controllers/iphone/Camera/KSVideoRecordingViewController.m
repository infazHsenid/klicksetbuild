//
//  KSVideoRecordingViewController.m
//  Klickset
//
//  Created by Pramuka Dias on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSVideoRecordingViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "KSUserAPI.h"

@interface KSVideoRecordingViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (strong, nonatomic) UIImagePickerController *picker;
@end

@implementation KSVideoRecordingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self captureVideo];
}

-(void)captureVideo{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        _picker = [[UIImagePickerController alloc] init];
        _picker.delegate = self;
        _picker.allowsEditing = YES;
        _picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        _picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        [self.parentViewController presentViewController:_picker animated:YES completion:nil];
    }
}

#pragma UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self.tabBarController setSelectedIndex:0];
    [self setMediaInfo:info];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self.tabBarController setSelectedIndex:0];
}

- (void)setMediaInfo:(NSDictionary *)mediaInfo {
    
    NSLog(@"mediaInfo %@",mediaInfo);
    NSString *selectedMediaType = mediaInfo[UIImagePickerControllerMediaType];
    if ([selectedMediaType isEqualToString:(NSString *) kUTTypeImage]) { // if mediatype is image
    }else if ([selectedMediaType isEqualToString:(NSString *) kUTTypeMovie]){ //if mediatype is video
        NSURL *videoUrl=(NSURL*)mediaInfo[UIImagePickerControllerMediaURL];
        NSString *path = [videoUrl path];
        [[KSUserAPI sharedUserAPI]postNewVideo:path passingCaption:@"nice video to be shared" success:^(NSMutableArray *videoArr) {
            
        } failure:^(NSMutableArray *errorArr) {
            
        }];
    }
}

@end
