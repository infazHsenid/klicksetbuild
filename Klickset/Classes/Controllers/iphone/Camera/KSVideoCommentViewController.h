//
//  KSVideoCommentViewController.h
//  Klickset
//
//  Created by Pramuka Dias on 7/6/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "SLKTextViewController.h"

@interface KSVideoCommentViewController : SLKTextViewController
@property BOOL isFromCommentButton;
@end
