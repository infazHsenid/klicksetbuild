//
//  KSVideoCommentViewController.m
//  Klickset
//
//  Created by Pramuka Dias on 7/6/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSVideoCommentViewController.h"
#import "KSVideoCommentTableViewCell.h"
#import "KSCommentTextView.h"
#import "KSComments.h"
#import "KSUser.h"
#import "KSProfileViewController.h"

#define USER_NAME_LABEL_FONT_SIZE ((IS_IPAD) ? 23.0f:(IS_IPhone6Plus) ? 18.0f:(IS_IPhone6) ? 17.0f:16.0f)
#define COMMENT_FONT_SIZE ((IS_IPAD) ? 17.0f:(IS_IPhone6Plus) ? 15.0f:(IS_IPhone6) ? 14.0f:13.0f)
static NSString *commentCellIdentifier = @"CommentCell";

@interface KSVideoCommentViewController ()
@property (nonatomic, strong) NSMutableArray *commentsArray;
@property (strong, nonatomic) NSIndexPath *editingIndexPath;
@end

@implementation KSVideoCommentViewController

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:UITableViewStylePlain];
    if (self) {
        [self initializeCommentsView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Comments";
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self configureCommentsView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.isFromCommentButton) {
        [self.textView becomeFirstResponder];
        self.isFromCommentButton=NO;
    }
}

-(void)initializeCommentsView{
    // Register a SLKTextView subclass, if you need any special appearance and/or behavior customisation.
    [self registerClassForTextView:[KSCommentTextView class]];
}

-(void)configureCommentsView{
//    create comments array with profile name,profile image and comment
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (int i = 0; i < 10; i++) {
        KSComments *comments = [KSComments new];
        comments.username = @"Michel Haden";
        comments.commentText = @"dolor sit er elit lamet, consectetaur cillium adipisicing pecu,😀 sed do eiusmod tempor incididunt ut labore et 😊.";
        comments.profileImageName=@"sample_profile_image";
        [array addObject:comments];
    }
    NSArray *reversedArray = [[array reverseObjectEnumerator] allObjects];
    self.commentsArray = [[NSMutableArray alloc] initWithArray:reversedArray];
//    comments view settings
    self.bounces = YES;
    self.shakeToClearEnabled = YES;
    self.keyboardPanningEnabled = YES;
    self.shouldScrollToBottomAfterKeyboardShows = NO;
    self.inverted = YES;
//  register KSVideoCommentTableViewCell
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.tableView registerClass:[KSVideoCommentTableViewCell class] forCellReuseIdentifier:commentCellIdentifier];
//    right button text
    [self.rightButton setTitle:@"Send" forState:UIControlStateNormal];
//    text view setings
    [self.textInputbar.editorTitle setTextColor:[UIColor darkGrayColor]];
    [self.textInputbar.editortLeftButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
    [self.textInputbar.editortRightButton setTintColor:[UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
    self.textInputbar.autoHideRightButton = YES;
    self.textInputbar.maxCharCount = 256;
    self.textInputbar.counterStyle = SLKCounterStyleSplit;
    self.textInputbar.counterPosition = SLKCounterPositionTop;
}

#pragma mark - UITableViewDataSource Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.commentsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self commentCellForRowAtIndexPath:indexPath];
}

- (KSVideoCommentTableViewCell *)commentCellForRowAtIndexPath:(NSIndexPath *)indexPath{
    KSVideoCommentTableViewCell *commentCell = (KSVideoCommentTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:commentCellIdentifier];
    if (!commentCell.textLabel.text) {
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(editComment:)];
        [commentCell addGestureRecognizer:longPress];
        [self addTapGestureToShowUserProfile:commentCell.titleLabel];
        [self addTapGestureToShowUserProfile:commentCell.thumbnailView];
    }
    KSComments *comment = self.commentsArray[indexPath.row];
    commentCell.titleLabel.text = comment.username;
    commentCell.bodyLabel.attributedText = [self getAttributedCommentText:comment.commentText withTimestamp:@"6h ago"];
    commentCell.thumbnailView.image=[UIImage imageNamed:comment.profileImageName];
    commentCell.thumbnailView.layer.shouldRasterize = YES;
    commentCell.thumbnailView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    commentCell.indexPath = indexPath;
    commentCell.usedForMessage = YES;
    commentCell.transform = self.tableView.transform;
    return commentCell;
}

#pragma mark - UITableViewDelegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    KSComments *comment = self.commentsArray[indexPath.row];
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    NSDictionary *titleAttributes = @{NSFontAttributeName: [UIFont fontWithName:KFONT_NEW size:USER_NAME_LABEL_FONT_SIZE],NSParagraphStyleAttributeName: paragraphStyle};
    NSDictionary *bodyAttributes = @{NSFontAttributeName: [UIFont fontWithName:KFONT_NEW size:COMMENT_FONT_SIZE],NSParagraphStyleAttributeName: paragraphStyle};
    CGFloat width = CGRectGetWidth(tableView.frame)-AVATAR_HEIGHT;
    width -= 25.0;
    CGRect titleBounds = [comment.username boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:titleAttributes context:NULL];
    NSString *commentText =[NSString stringWithFormat:@"%@         %@",comment.commentText,@"6h ago"];
    CGRect bodyBounds = [commentText boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:bodyAttributes context:NULL];
    if (comment.commentText.length == 0) {
        return 0.0;
    }
    CGFloat height = CGRectGetHeight(titleBounds);
    height += CGRectGetHeight(bodyBounds);
    height += 40.0;
    if (height < kMessageTableViewCellMinimumHeight) {
        height = kMessageTableViewCellMinimumHeight;
    }
    return height;
}

-(void)addTapGestureToShowUserProfile:(id)element{
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navigateToUserProfileViewWithUser:)];
    [element addGestureRecognizer:tapGesture];
}

-(NSAttributedString *)getAttributedCommentText:(NSString *)comment withTimestamp:(NSString *)timeStamp{//
    NSMutableAttributedString *commentAtrString=[[KSCommonUtils getAttributedText:comment withFontName:KFONT_NEW withFontSize:COMMENT_FONT_SIZE textColor:[UIColor colorWithRed:88.0/255.0 green:90.0/255.0 blue:90.0/255.0 alpha:1.0]] mutableCopy];
    NSAttributedString *timeStampAtrString = [KSCommonUtils getAttributedText:[NSString stringWithFormat:@" %@",timeStamp] withFontName:KFONT_NEW withFontSize:COMMENT_FONT_SIZE-2 textColor:[UIColor colorWithRed:150.0/255.0 green:150.0/255.0 blue:150.0/255.0 alpha:1.0]];
    [commentAtrString appendAttributedString:timeStampAtrString];
    return commentAtrString;
}

#pragma mark edit comment
- (void)editComment:(UIGestureRecognizer *)gesture{
    KSVideoCommentTableViewCell *cell = (KSVideoCommentTableViewCell *)gesture.view;
    _editingIndexPath=cell.indexPath;
    KSComments *comment = self.commentsArray[cell.indexPath.row];
    [self editText:comment.commentText];
    [self.tableView scrollToRowAtIndexPath:cell.indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

#pragma mark - Overriden Methods

- (void)didPressRightButton:(id)sender{
    // Notifies the view controller when the right button's action has been triggered, manually or by using the keyboard return key.
    KSComments *comment = [KSComments new];
    comment.username = @"Michel Haden";
    comment.commentText=[self.textView.text copy];
    comment.profileImageName=@"sample_profile_image";
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewRowAnimation rowAnimation = self.inverted ? UITableViewRowAnimationBottom : UITableViewRowAnimationTop;
    UITableViewScrollPosition scrollPosition = self.inverted ? UITableViewScrollPositionBottom : UITableViewScrollPositionTop;
    [self.tableView beginUpdates];
    [self.commentsArray insertObject:comment atIndex:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:rowAnimation];
    [self.tableView endUpdates];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:YES];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [super didPressRightButton:sender]; // keep this statement at the bottom of this scope.do not move to top.this remove the text of text view.
}

- (void)didChangeKeyboardStatus:(SLKKeyboardStatus)status{
    // Notifies the view controller that the keyboard changed status.
}

- (void)textWillUpdate{
    [super textWillUpdate];
    // Notifies the view controller that the text will update.
}

- (void)textDidUpdate:(BOOL)animated{
    [super textDidUpdate:animated];
    // Notifies the view controller that the text did update.
}

- (void)didPressLeftButton:(id)sender{
    [super didPressLeftButton:sender];
    // Notifies the view controller when the left button's action has been triggered, manually.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//The key for which to enable text caching.
- (NSString *)keyForTextCaching{
    return [[NSBundle mainBundle] bundleIdentifier];
}

- (void)willRequestUndo{
    [super willRequestUndo];
    // Notifies the view controller when a user did shake the device to undo the typed text
}

- (void)didCommitTextEditing:(id)sender{
    // Notifies the view controller when tapped on the right "Accept" button for commiting the edited text
    KSComments *comment = self.commentsArray[_editingIndexPath.row];
    KSComments *editedComment =[KSComments new];
    editedComment.username = comment.username;
    editedComment.commentText=[self.textView.text copy];
    editedComment.profileImageName=comment.profileImageName;
    [self.commentsArray removeObjectAtIndex:_editingIndexPath.row];
    [self.commentsArray insertObject:editedComment atIndex:_editingIndexPath.row];
    [self.tableView reloadData];
    [super didCommitTextEditing:sender];
}

- (void)didCancelTextEditing:(id)sender{
    [super didCancelTextEditing:sender];
    // Notifies the view controller when tapped on the left "Cancel" button
}

- (BOOL)canPressRightButton{
    return [super canPressRightButton];
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [super scrollViewDidScroll:scrollView];
}

#pragma mark - UITextViewDelegate Methods
- (BOOL)textView:(SLKTextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    return [super textView:textView shouldChangeTextInRange:range replacementText:text];
}

// navigate to user profile view
- (void)navigateToUserProfileViewWithUser:(KSUser *)user{
    [self.textView resignFirstResponder];
    KSProfileViewController *profileView = (KSProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileView"];
    [self.navigationController pushViewController:profileView animated:YES];
}

@end
