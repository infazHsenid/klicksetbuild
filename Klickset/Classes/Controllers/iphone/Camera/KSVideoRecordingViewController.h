//
//  KSVideoRecordingViewController.h
//  Klickset
//
//  Created by Pramuka Dias on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSAbstractUIViewController.h"

@interface KSVideoRecordingViewController : KSAbstractUIViewController

@end
