//
//  KSSoundBoardViewController.m
//  Klickset
//
//  Created by Infaz Ariff on 7/9/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSSoundBoardViewController.h"
#import "KSSoundBoardCell.h"
#import "FlatButton.h"
#define SEGMENT_CTRL_LEADIND_TRAILING_SPACE ((IS_IPAD) ?72.0f:(IS_IPhone6Plus) ? 45.0f:(IS_IPhone6) ? 42.0f:32.0f)
#define HEART_BUTTON_IMAGE ((IS_IPAD) ?@"heart-btn":(IS_IPhone6Plus) ? @"cell_six_heart-btn":(IS_IPhone6) ? @"cell_six_heart-btn":@"cell_heart-btn")
@interface KSSoundBoardViewController ()
@property (weak, nonatomic) IBOutlet UITableView *soundTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *soundBoardSegmentController;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *segmentCtrlLeadindAndTrailingSpace;
@property (nonatomic, strong) NSIndexPath *currentPlayingIndexPath;
@property (nonatomic, strong) NSIndexPath *priviousPlayingIndexPath;
@property (strong, nonatomic) FlatButton *playButton;
@end

@implementation KSSoundBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupSegmentController];
    [self setupTableViewRefresh:self.soundTableView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    for (NSLayoutConstraint *constraint in _segmentCtrlLeadindAndTrailingSpace) {
        constraint.constant=SEGMENT_CTRL_LEADIND_TRAILING_SPACE;
    }
}

-(void)setupSegmentController{
    _soundBoardSegmentController.selectedSegmentIndex = 0;
    NSDictionary *normalAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], NSForegroundColorAttributeName,nil];
    [_soundBoardSegmentController setTitleTextAttributes:normalAttributes forState:UIControlStateNormal];
    normalAttributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil];
    [_soundBoardSegmentController setTitleTextAttributes:normalAttributes forState:UIControlStateSelected];
    _soundBoardSegmentController.tintColor = RED_SEGEMENT_TINT_COLOR;
}

- (IBAction)segmentControllerChanged:(id)sender {
    [self.soundTableView reloadData];
}

#pragma mark TabelView DataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  ((IS_IPAD) ? 55.0f:(IS_IPhone6Plus) ? 48.0f:(IS_IPhone6) ? 43.0f:38.0f);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *soundCellIdentifier = @"soundCellId";
    KSSoundBoardCell *cell = [_soundTableView dequeueReusableCellWithIdentifier:soundCellIdentifier];
    if (cell==nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"KSSoundBoardCell" owner:self options:nil] objectAtIndex:0];
        [cell.likeButton setup];
        cell.SoundBoardCellDelegate = (id)self;
        cell.delegate=(id)self;
    }
    [self configureCellSlideGesture:cell];
    cell.indexPath=indexPath;
    [cell.likeButton setSelected:NO];
    [cell cellForRowAtIndexPath:indexPath withSelectedPlayingIndexPath:self.currentPlayingIndexPath withPlayButton:self.playButton];
    cell.soundNameLabel.text=@"Sound Board Sample Text";
    [cell.soundNameLabel restartLabel];
    return cell;
}

#pragma mark TabelView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark Cell slide gesture recognizer
- (void)configureCellSlideGesture:(KSSoundBoardCell *)cell{
    UIView *heartView = [self viewWithImageName:HEART_BUTTON_IMAGE];
    UIColor *activeBackgroundColor = RED_THEME_COLOR;
    cell.firstTrigger = 0.1;
    [cell setSwipeGestureWithView:heartView color:activeBackgroundColor mode:MCSwipeTableViewCellModeSwitch state:MCSwipeTableViewCellState3 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
        [self didTriggerPullingBlock:(KSSoundBoardCell *)cell];
    }];
}

- (UIView *)viewWithImageName:(NSString *)imageName {
    UIImage *image = [UIImage imageNamed:imageName];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    imageView.contentMode = UIViewContentModeCenter;
    return imageView;
}

#pragma mark - MCSwipeTableViewCellDelegate

- (void)didTriggerPullingBlock:(KSSoundBoardCell *)cell{
        UIColor *activeColor;
        [cell.likeButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        if (!cell.likeButton.isSelected) {
            activeColor = RED_THEME_COLOR;
        }else{
            activeColor = [UIColor lightGrayColor];
        }
    UIView *heartView = [self viewWithImageName:HEART_BUTTON_IMAGE];
    UIColor *activeBackgroundColor = activeColor;
    [cell setSwipeGestureWithView:heartView color:activeBackgroundColor mode:MCSwipeTableViewCellModeSwitch state:MCSwipeTableViewCellState3 completionBlock:^(MCSwipeTableViewCell *cell, MCSwipeTableViewCellState state, MCSwipeTableViewCellMode mode) {
        [self didTriggerPullingBlock:(KSSoundBoardCell *)cell];
    }];
}

#pragma mark KSSoundBoardCell Delegate
- (void)playButtonTappedWithIndexPath:(NSIndexPath*)indexPath withPlayButton:(FlatButton *)playButton{
    _priviousPlayingIndexPath=_currentPlayingIndexPath;
    _currentPlayingIndexPath=indexPath;
    _playButton=playButton;
    NSArray *indexPathArray = (_priviousPlayingIndexPath)?@[_priviousPlayingIndexPath,_currentPlayingIndexPath]:@[_currentPlayingIndexPath];
    [self.soundTableView reloadRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationFade];
}

@end
