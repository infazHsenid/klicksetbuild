//
//  KSSearchViewController.m
//  Klickset
//
//  Created by Infaz Ariff on 7/10/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSSearchViewController.h"
#import "KSSearchTableViewCell.h"

@interface KSSearchViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *searchTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) NSMutableArray *trendingTagsArray;
@property (strong, nonatomic) NSMutableArray *popularTagsArray;
@end

@implementation KSSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupTagsArray];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}

-(void)setupTagsArray{
    self.trendingTagsArray =[NSMutableArray arrayWithObjects:@"DancingWithBff",@"DancingStarSS01",@"SuperSingerSS02",@"HomeMagic",@"HomeMadeCake",@"StreetRace", nil];
    self.popularTagsArray =[NSMutableArray arrayWithObjects:@"SingingSongs",@"FunnyDogs",@"StubbornCats",@"RaspberryPI",@"StageDrama",@"BestFreeKicks" ,nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ((IS_IPAD) ? 60.0f:(IS_IPhone6Plus) ? 50.0f:(IS_IPhone6) ? 45.0f:40.0f);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self numberOfRowsInSection];
}

-(NSInteger)numberOfRowsInSection{
    if ([self.trendingTagsArray count] < [self.popularTagsArray count]) {
        return [self.popularTagsArray count];
    }
    return [self.trendingTagsArray count];
}

#pragma mark TabelView DataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *searchCellHeaderIdentifier = @"searchCellIdentifier";
    KSSearchTableViewCell *cell = [_searchTableView dequeueReusableCellWithIdentifier:searchCellHeaderIdentifier];
    if (!cell) {
        NSArray *nibArr = [[NSBundle mainBundle]loadNibNamed:@"KSSearchTableViewCell" owner:self options:nil];
        cell = [nibArr objectAtIndex:0];
    }
    [self configureSearchCell:cell ForRowAtIndexPath:indexPath];
    return cell;
}

-(void)configureSearchCell:(KSSearchTableViewCell *)cell ForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_trendingTagsArray.count > indexPath.row) {
        [cell.trendingTagsValuesButton setTitle:[self setHashSymbolToText:[self.trendingTagsArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        [cell.trendingTagsValuesButton addTarget:self action:@selector(trendingButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [cell.trendingTagsValuesButton setHidden:YES];
    }
    if (_popularTagsArray.count > indexPath.row) {
        [cell.popularTagsValuesButton setTitle:[self setHashSymbolToText:[self.popularTagsArray objectAtIndex:indexPath.row]] forState:UIControlStateNormal];
        [cell.popularTagsValuesButton addTarget:self action:@selector(popularButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        [cell.popularTagsValuesButton setHidden:YES];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    KSSearchTableViewCell* headerView=[[[NSBundle mainBundle]loadNibNamed:@"KSSearchTableViewCell" owner:self options:nil] objectAtIndex:1];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return ((IS_IPAD) ? 50.0f:(IS_IPhone6Plus) ? 40.0f:(IS_IPhone6) ? 35.0f:30.0f);
}

-(NSString *)setHashSymbolToText:(NSString *)tagName{
    NSString *hashTage=[NSString stringWithFormat:@"#%@",tagName];
    return hashTage;
}

-(void)trendingButtonAction:(id)sender{
    NSString *selectedTrendingTag =[self.trendingTagsArray objectAtIndex:[self getSelectedIndexPath:sender].row];
    NSLog(@"%@",selectedTrendingTag);
}

-(void)popularButtonAction:(id)sender{
    NSString *selectedPopularTag =[self.popularTagsArray objectAtIndex:[self getSelectedIndexPath:sender].row];
    NSLog(@"%@",selectedPopularTag);
}

-(NSIndexPath *)getSelectedIndexPath:(id)sender{
    CGPoint position = [sender convertPoint:CGPointZero toView:self.searchTableView];
    NSIndexPath *indexPath = [self.searchTableView indexPathForRowAtPoint:position];
    return indexPath;
}

@end
