//
//  KSCommentsViewController.h
//  Klickset
//
//  Created by Infaz Ariff on 7/3/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSAbstractUIViewController.h"

@interface KSCommentsViewController : KSAbstractUIViewController

@end
