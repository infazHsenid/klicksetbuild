//
//  KSHomeViewController.h
//  Klickset
//
//  Created by Pramuka Dias on 6/8/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSAbstractUIViewController.h"
typedef NS_ENUM(NSInteger, NavigationButton) {
    MIC = 10,
    NOTIFICATION,
    ALL_SHARE,
    SEARCH
};
@interface KSHomeViewController : KSAbstractUIViewController <UITableViewDataSource,UITableViewDelegate>
@end
