//
//  KSCommentsViewController.m
//  Klickset
//
//  Created by Infaz Ariff on 7/3/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSCommentsViewController.h"
#import "KSCommentsTableCell.h"
#import "KSUser.h"

@interface KSCommentsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
@end

@implementation KSCommentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureNavigationwithTitle:@"Comments"];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CommentsTableDelegate method
- (void)userProfileButtonTapped:(KSUser *)user{
    [self navigateToUserProfileViewWith:user];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return ((IS_IPAD) ? 80.0f:(IS_IPhone6Plus) ? 75.0f:(IS_IPhone6) ? 65.0f:55.0f);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

#pragma mark TabelView DataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *commentCellHeaderIdentifier = @"commentCellIdentifier";
    KSCommentsTableCell *cell = [_commentsTableView dequeueReusableCellWithIdentifier:commentCellHeaderIdentifier];
    if (!cell) {
        NSArray *nibArr = [[NSBundle mainBundle]loadNibNamed:@"KSCommentsTableCell" owner:self options:nil];
        cell = [nibArr objectAtIndex:0];
        [cell addValuesForCommentsTableCell];
    }
    return cell;
}


@end
