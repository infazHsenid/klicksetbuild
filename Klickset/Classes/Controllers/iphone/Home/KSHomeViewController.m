//
//  KSHomeViewController.m
//  Klickset
//
//  Created by Pramuka Dias on 6/8/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSHomeViewController.h"
#import "KSHomeVideoTableCell.h"
#import "KSUserAPI.h"
#import "KSVideo.h"
#import "GUIPlayerView.h"
#import <UIImageView+AFNetworking.h>
#import "KSAppDelegate.h"
#import "KSSoundBoardViewController.h"
#import "KSSearchViewController.h"
#import "MWPhotoBrowser.h"
#import "KSPhotoBrowser.h"
#import "NSDate+NVTimeAgo.h"
#import "CRGradientNavigationBar.h"
@interface KSHomeViewController () <GUIPlayerViewDelegate,UITabBarControllerDelegate,MWPhotoBrowserDelegate>{
    KSPhotoBrowser *browser;
}
@property (weak, nonatomic) IBOutlet UITableView *homeTableView;
@property (strong, nonatomic) KSHomeVideoTableCell *videoCell;
@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *thumbs;
@property (nonatomic, strong) NSMutableArray *selections;
@end

#define EXTRA_HEIGHT_FOR_ROW 10
#define ESTIMATED_ROW_HEIGHT ((IS_IPAD) ? 558.0f:(IS_IPhone6Plus) ? 504.0f:(IS_IPhone6) ? 461.0f:388.0f)
#define VIDEO_DESCRIPTION_FONT ((IS_IPAD) ? 17.0f:(IS_IPhone6Plus) ? 15.0f:(IS_IPhone6) ? 14.0f:13.0f)
@implementation KSHomeViewController
{
    NSMutableArray *videoDataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getVideos];
    [self changeTabBar];
    self.tabBarController.delegate = self;
    // Do any additional setup after loading the view.
    self.homeTableView.estimatedRowHeight = ESTIMATED_ROW_HEIGHT;
    [self setupTableViewRefresh:self.homeTableView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configureNavigation];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self videoPlayerPause:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return videoDataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *tableIdentifier = @"tableIdentifier";
    KSHomeVideoTableCell *cell = [self.homeTableView dequeueReusableCellWithIdentifier:tableIdentifier];
    KSVideo *videos = [videoDataArray objectAtIndex:indexPath.row];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"KSHomeVideoCell" owner:self options:nil]objectAtIndex:0];
        cell.videoCellDelegate = (id)self;
        [cell setUpanimationForButtons];
    }
    [cell.userNameLabel setText:videos.user.username];
    [cell.postedTimeLabel setText:[[self getDateFromString:videos.createdDate] formattedAsTimeAgo]];
    [cell.profilePictureImageView setImageWithURL:[NSURL URLWithString:videos.user.image.photo_thumb] placeholderImage:[UIImage imageNamed:@"user_placeholder"]];
    [cell.videoDescriptionTextView setText:videos.videoDescription];
    [cell setTextViewHeight];
    [cell setValuesForLikesLabel:videos.likesCount CommentsLabel:videos.commentsCount ViewsLabel:@"10"];
    [cell setUser:videos.user];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
//        cell.playerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:@"https://s3.amazonaws.com/adplayer/colgate.mp4"]];
//        cell.playerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:videos.videoPath]];
        AVAsset *avAsset = [AVAsset assetWithURL:[NSURL URLWithString:videos.videoPath]];
        cell.playerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (!cell.avPlayer) {
                cell.avPlayer = [AVPlayer playerWithPlayerItem:cell.playerItem];
            }else{
                [cell.avPlayer replaceCurrentItemWithPlayerItem:cell.playerItem];
            }
            if (!cell.avPlayerLayer) {
                cell.avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:cell.avPlayer];
                cell.avPlayerLayer.frame = cell.videoPlayerView.layer.bounds;
                cell.avPlayerLayer.videoGravity = AVLayerVideoGravityResize;
                [cell.videoPlayerView.layer addSublayer: cell.avPlayerLayer];
                if (indexPath.row == 0) {
                    cell.avPlayer.muted = NO;
                    [cell.avPlayer play];
                }
                [self addVolumeButtonCell:cell];
            }
            [self setAVPlayerNotificationsWithCell:cell];
        });
    });
    cell.indexPath = indexPath;
    [cell updateConstraintsIfNeeded];
    return cell;
}

#pragma mark AVPlayer Notifications
-(void)setAVPlayerNotificationsWithCell:(KSHomeVideoTableCell *)videoCell{
    [[NSNotificationCenter defaultCenter] removeObserver:videoCell name:AVPlayerItemDidPlayToEndTimeNotification object:[videoCell.avPlayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:videoCell
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[videoCell.avPlayer currentItem]];
}

- (void)addVolumeButtonCell:(KSHomeVideoTableCell *)cell{
    cell.volumeButton = [[FlatButton alloc]initWithFrame:CGRectMake(10, 10, 100, 100)];
    [cell.volumeButton setImage:[UIImage imageNamed:@"volume-mute-icon"] forState:UIControlStateNormal];
    [cell.volumeButton setImage:[UIImage imageNamed:@"volumed"] forState:UIControlStateSelected];
    [cell.volumeButton setup];
    [cell.volumeButton addTarget:cell action:@selector(volumeButtonTappedWithIndexPath:) forControlEvents:UIControlEventTouchUpInside];
    [cell.videoPlayerView addSubview:cell.volumeButton];
    [cell.volumeButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    [cell.videoPlayerView setTranslatesAutoresizingMaskIntoConstraints:NO];
    UIView *popView = cell.volumeButton;
    UIView *sourceView = cell.videoPlayerView;
    NSDictionary *views = NSDictionaryOfVariableBindings(popView);
    int width = (IS_IPhone6Plus)?32.0:(IS_IPhone6)?28.0:25.0;
    int height = width-4;
    NSDictionary *metrics = @{@"height":@(height),@"width":@(width)};
    [sourceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[popView(height)]-7-|" options:0 metrics:metrics views:views]];
    [sourceView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[popView(width)]-7-|" options:0 metrics:metrics views:views]];
}

- (void)videoPlayerPause:(BOOL)shouldPause{
    if (shouldPause) {
        for (KSHomeVideoTableCell *cell in _homeTableView.visibleCells) {
            [self notifyCell:cell CompletelyVisible:!shouldPause];
        }
    }else{
        [self scrollViewDidScroll:(UIScrollView *)_homeTableView.superview];
    }
}

#pragma mark ScrollView delegate methods
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    NSArray* cells = _homeTableView.visibleCells;
    
    NSUInteger cellCount = [cells count];
    if (cellCount == 0)
        return;
    
    // Check the visibility of the first cell
    [self checkVisibilityOfCell:[cells firstObject] inScrollView:aScrollView];
    if (cellCount == 1)
        return;
    
    // Check the visibility of the last cell
    [self checkVisibilityOfCell:[cells lastObject] inScrollView:aScrollView];
    if (cellCount == 2)
        return;
    
    // All of the rest of the cells are visible: Loop through the 2nd through n-1 cells
    for (NSUInteger i = 1; i < cellCount - 1; i++)
        [self notifyCell:[cells objectAtIndex:i] CompletelyVisible:YES];
}

#pragma mark auto play video methods
- (void)checkVisibilityOfCell:(KSHomeVideoTableCell *)cell inScrollView:(UIScrollView *)aScrollView {
    CGRect cellRect = [aScrollView convertRect:cell.frame toView:aScrollView.superview];
    
    if (CGRectContainsRect(aScrollView.frame, cellRect))
        [self notifyCell:cell CompletelyVisible:YES];
    else
        [self notifyCell:cell CompletelyVisible:NO];
}

- (void)notifyCell:(KSHomeVideoTableCell *)cell CompletelyVisible:(BOOL)isVisible{
    if (isVisible) {
        [cell.avPlayer play];
    }else{
        [cell.avPlayer pause];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat rowHeight   = [self getTableCellHeightForRowAtIndexPath:indexPath];
    if ([NSIndexPath indexPathForRow:videoDataArray.count-1 inSection:0].row == indexPath.row) { //get the last cell and do not add extra height for last cell
        return rowHeight;
    }
    return rowHeight+EXTRA_HEIGHT_FOR_ROW ; // add extra height to row to show seperation of cell.
}

// get table cell height
-(CGFloat)getTableCellHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    KSVideo *videos = [videoDataArray objectAtIndex:indexPath.row];
    CGFloat cellHeight = ESTIMATED_ROW_HEIGHT;
    CGFloat textViewHeight = [self textViewHeightForText:videos.videoDescription andWidth:DEVICE_WIDTH];;
    return cellHeight +textViewHeight;
}

- (CGFloat)textViewHeightForText:(NSString*)text andWidth:(CGFloat)width {
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setText:text];
    [calculationView setFont:[UIFont fontWithName:KFONT_NEW size:VIDEO_DESCRIPTION_FONT]];
    [calculationView setTextColor:[UIColor colorWithRed:88.0/255.0 green:90.0/255.0 blue:90.0/255.0 alpha:1.0]];
    [calculationView sizeToFit];
    CGSize newSize = [calculationView sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    CGRect newFrame = calculationView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, width),newSize.height);
    return   newFrame.size.height;
}

- (void)refreshControlValueChanged:(id)sender{
    [self.refreshControl beginRefreshing];
    // simulate loading time
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self getVideos];
        [self.refreshControl endRefreshing];
    });
}

-(void)getVideos{
    [self showLoading];
    [[KSUserAPI sharedUserAPI]getVideoForLastID:@"20" andRecordsPerPage:@"20" success:^(NSMutableArray *array) {
        videoDataArray = array;
        [self.homeTableView reloadData];
        [self stopLoading];
    } failure:^(NSError *error) {
        [self stopLoading];
        [self showAlertViewWithTitle:nil andSubTitle:@"Network Not Found" andCloseButtonTitle:@"Ok" forType:alert_Error];
    }];
}

// convert string to nsadte
-(NSDate *)getDateFromString:(NSString *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *formatteredDate = [formatter dateFromString:date];
    return formatteredDate;
}

// create recording button in tab bar
-(void)changeTabBar{
    float topBottomSpace = 2.0;
    float extraHeight = 11.0;
    float buttonSize = ([[self tabBarController]tabBar].frame.size.height -topBottomSpace)+extraHeight;
    float buttonxAxis = self.view.frame.size.width/topBottomSpace- (buttonSize/topBottomSpace);
    float buttonyAxis = -4.0;
    UIButton *tabBarButton = [[UIButton alloc]initWithFrame:CGRectMake(buttonxAxis, buttonyAxis, buttonSize, buttonSize)];
    [tabBarButton setBackgroundImage:[UIImage imageNamed:@"record_button_animation0"]forState:UIControlStateNormal];
    [tabBarButton setBackgroundImage:[UIImage animatedImageNamed:@"record_button_animation" duration:3.0]forState:UIControlStateHighlighted];
    [tabBarButton setBackgroundImage:[UIImage animatedImageNamed:@"record_button_animation" duration:3.0]forState:UIControlStateSelected];
    [tabBarButton addTarget:self action:@selector(tabBarButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [[[self tabBarController]tabBar] addSubview: tabBarButton];
}

//cofiguer navigation bar
-(void)configureNavigation{
    self.navigationController.navigationBar.translucent = NO;
    self.tabBarController.navigationItem.hidesBackButton=YES;
    [self.navigationController.navigationBar setHidden:NO];
//    self.navigationController.navigationBar.barTintColor = RED_THEME_COLOR;
    self.tabBarController.navigationItem.title =@"";
    self.tabBarController.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
//    add navigation bar buttons
    UIBarButtonItem *micButton =[self createNavigationBarButtonWithImageName:@"mic-icon.png" buttonFrame:CGRectMake(0,0,15,25)withTag:10] ;
    UIBarButtonItem *notifyButton =[self createNavigationBarButtonWithImageName:@"notification-icon.png" buttonFrame:CGRectMake(0,0,30,25)withTag:11];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    item.width =20;
    UIBarButtonItem *shareButton =[self createNavigationBarButtonWithImageName:@"share-icon.png" buttonFrame:CGRectMake(0,0,18,23)withTag:12] ;
    UIBarButtonItem *searchButton =[self createNavigationBarButtonWithImageName:@"Search-icon.png" buttonFrame:CGRectMake(0,0,23,23)withTag:13] ;
    self.tabBarController.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:micButton,item,notifyButton,nil];
    self.tabBarController.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:searchButton,item,shareButton,nil];
    NSArray *colors = [NSArray arrayWithObjects:(id)[UIColor colorWithRed:159.0/255.0 green:0.0 blue:22.0/255.0 alpha:1.0].CGColor, (id)[UIColor colorWithRed:209.0/255.0 green:0.0 blue:85.0/255.0 alpha:1.0].CGColor, nil];
    [(CRGradientNavigationBar *)[self.navigationController navigationBar] setBarTintGradientColors:colors];
}

-(UIBarButtonItem *)createNavigationBarButtonWithImageName:(NSString *)imagename buttonFrame:(CGRect)buttonFrame withTag:(int)tag{
    UIButton *button =[[UIButton alloc] init];
    [button setBackgroundImage:[UIImage imageNamed:imagename] forState:UIControlStateNormal];
    [button.imageView setContentMode:UIViewContentModeScaleAspectFit];
    button.frame = buttonFrame;
    button.tag = tag;
    [button addTarget:self action:@selector(navigationButtonsAction:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButton =[[UIBarButtonItem alloc] initWithCustomView:button];
    return barButton;
}

-(void)tabBarButtonClicked{
    [[self tabBarController]setSelectedIndex:2];
}

#pragma mark HomeVideoCellDelegate method
- (void)videoPlayerTappedWithIndexPath:(NSIndexPath*)indexPath{
    
}

- (void)userProfileImageTappedWithUser:(KSUser *)user{
    [self navigateToUserProfileViewWith:user];
}

- (void)commentTappedAction:(KSUser *)user isFromButton:(BOOL)isFromButton{
     [self navigateToUserCommentsViewWith:user isFromButton:isFromButton];
}

- (void)shareButtonTappedWithIdexPath:(NSIndexPath *)indexPath{
    KSVideo *videos = [videoDataArray objectAtIndex:indexPath.row];
    NSArray *videoData = [NSArray arrayWithObject:videos.videoPath];
    UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:videoData applicationActivities:nil];
    NSArray *excludedActivities = @[UIActivityTypePostToWeibo,
                                    UIActivityTypeMail,
                                    UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
                                    UIActivityTypeAssignToContact,                                     UIActivityTypePostToFlickr,
                                    UIActivityTypePostToVimeo,UIActivityTypeAirDrop, UIActivityTypePostToTencentWeibo];
    controller.excludedActivityTypes = excludedActivities;
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)navigationButtonsAction:(id)sender{
    UIButton *item = (UIButton *)sender;
    switch (item.tag) {
        case MIC:
            NSLog(@"hit mic icon");
            [self navigateToSoundBoardView];
            break;
        case NOTIFICATION:
            NSLog(@"hit notify icon");
            break;
        case ALL_SHARE:
            NSLog(@"hit all share icon");
            [self createVideosForAllshare];
            break;
        case SEARCH:
            NSLog(@"hit search icon");
            [self navigateToSearchView];
            break;
        default:
            break;
    }
    NSLog(@"hit buttons action");
}

- (void)navigateToSoundBoardView{
    KSSoundBoardViewController *soundView = (KSSoundBoardViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SoundBoardView"];
    [self.navigationController pushViewController:soundView animated:YES];
}

- (void)navigateToSearchView{
    KSSearchViewController *searchView = (KSSearchViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SearchView"];
    [self.navigationController pushViewController:searchView animated:YES];
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    if (tabBarController.selectedIndex == 0) {
        [self videoPlayerPause:NO];
    }else{
        [self videoPlayerPause:YES];
    }
}

//Create All share videos View
- (void)createVideosForAllshare{
    if (videoDataArray.count != _photos.count) {
        _photos = [[NSMutableArray alloc] init];
        _thumbs = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < videoDataArray.count; i ++) {
            KSVideo *videos = [videoDataArray objectAtIndex:i];
            MWPhoto *photo = [MWPhoto photoWithURL:[[NSURL alloc] initWithString:videos.videoThumbPath]];
            photo.videoURL = [[NSURL alloc] initWithString:videos.videoPath];
            [_photos addObject:photo];
            MWPhoto *thumb = [MWPhoto photoWithURL:[[NSURL alloc] initWithString:videos.videoThumbPath]];
            thumb.isVideo = YES;
            [_thumbs addObject:thumb];
        }
    }
    
    if (_photos.count > 0) {
        // Create browser
        browser = [[KSPhotoBrowser alloc] initWithDelegate:self];
        browser.displayActionButton = NO;
        browser.displayNavArrows = YES;
        browser.displaySelectionButtons = YES;
        browser.alwaysShowControls = YES;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = YES;
        browser.startOnGrid = YES;
        browser.enableSwipeToDismiss = NO;
        browser.allShareDelegate = (id)self;
        _selections = [NSMutableArray new];
        for (int i = 0; i < _photos.count; i++) {
            [_selections addObject:[NSNumber numberWithBool:NO]];
        }
        [self.navigationController pushViewController:browser animated:YES];
    }
}

#pragma mark - MWPhotoBrowserDelegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < _thumbs.count)
        return [_thumbs objectAtIndex:index];
    return nil;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}

- (BOOL)photoBrowser:(MWPhotoBrowser *)photoBrowser isPhotoSelectedAtIndex:(NSUInteger)index {
    if (_selections.count > 0) {
        return [[_selections objectAtIndex:index] boolValue];
    }return NO;
}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index selectedChanged:(BOOL)selected {
    [_selections replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:selected]];
    NSLog(@"Photo at index %lu selected %@", (unsigned long)index, selected ? @"YES" : @"NO");
}

#pragma mark AllShare Delegate Method
- (void)allShareButtonTapped{
    NSMutableArray *shareArray = [NSMutableArray array];
    if (self.selections.count > 0) {
        NSLog(@"%@",self.selections);
        for (int i = 0; i < self.selections.count; i++) {
            if ([[self.selections objectAtIndex:i]boolValue] == YES) {
                MWPhoto *photo = [self.photos objectAtIndex:i];
                [shareArray addObject:photo.videoURL];
            }
        }
    }
    
    if (shareArray.count > 0) {
        UIActivityViewController *controller = [[UIActivityViewController alloc] initWithActivityItems:shareArray applicationActivities:nil];
        NSArray *excludedActivities = @[UIActivityTypePostToWeibo,
                                        UIActivityTypeMail,
                                        UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
                                        UIActivityTypeAssignToContact,                                     UIActivityTypePostToFlickr,
                                        UIActivityTypePostToVimeo,UIActivityTypeAirDrop, UIActivityTypePostToTencentWeibo];
        controller.excludedActivityTypes = excludedActivities;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
