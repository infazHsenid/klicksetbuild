//
//  KSSettingsViewController.m
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSSettingsViewController.h"
#import "KSProfileViewController.h"
@interface KSSettingsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
@property (strong, nonatomic) NSMutableArray *settingTableArray;
@property (strong, nonatomic) KSProfileViewController *profileView;
@end

@implementation KSSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadTableDataArray];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self configureNavigation];
}

//cofiguer navigation bar
-(void)configureNavigation{
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setHidden:NO];
    self.tabBarController.navigationItem.title =@"Settings";
    self.tabBarController.navigationItem.leftBarButtonItems=nil;
    self.tabBarController.navigationItem.rightBarButtonItems=nil;
}

//Load the tableData array with data
- (void)loadTableDataArray{
    NSArray *upperRowsArray = [NSArray arrayWithObjects:@"About Klickset",@"Invite Friends", nil];
    NSArray *lowerRowsArray = [NSArray arrayWithObjects:@"Profile",@"My Post",@"Account Settings",@"Alerts",@"Video Settings", nil];
    _settingTableArray = [NSMutableArray array];
    [_settingTableArray addObject:upperRowsArray];
    [_settingTableArray addObject:lowerRowsArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark TabelView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //Last row should show only one row with Logout option
    return (section == 2)?1:[[_settingTableArray objectAtIndex:section]count];
}

#pragma mark TabelView DataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *settingCellIdentifier = @"settingCell";
    
    UITableViewCell *settingCell = [_settingsTableView dequeueReusableCellWithIdentifier:settingCellIdentifier];
    if (!settingCell) {
        settingCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:settingCellIdentifier];
    }
    if (indexPath.section != _settingTableArray.count) {
        settingCell.textLabel.text = [[_settingTableArray objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    }else{
        settingCell.textLabel.text = @"Logout";
        settingCell.textLabel.textColor = [UIColor redColor];
    }
    settingCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return settingCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==1 && indexPath.row==0) {
//        [self performSegueWithIdentifier:@"ProfileView" sender:nil];
        [self navigateToUserProfileViewWith:nil];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"ProfileView"]){
        _profileView = [KSProfileViewController new];
        _profileView=[segue destinationViewController];
    }
}

@end
