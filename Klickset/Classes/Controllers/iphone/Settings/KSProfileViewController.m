//
//  KSProfileViewController.m
//  Klickset
//
//  Created by Pramuka Dias on 6/18/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSProfileViewController.h"
#import "KSProfileThumbCell.h"
#import "KSProfileTableHeader.h"
#import "KSUserAPI.h"
#import "KSCommonUtils.h"
#import "KSVideo.h"
#import <UIButton+AFNetworking.h>
#import <UIImageView+AFNetworking.h>
#import "NSString+KSTextString.h"
#import "KSAppDelegate.h"

@interface KSProfileViewController ()
@property (weak, nonatomic) IBOutlet UITableView *profileTableView;
@property (strong, nonatomic) NSMutableArray *videoArray;
@property (strong, nonatomic) NSMutableArray *splittedvideoArray;
@end

@implementation KSProfileViewController
#define ESTIMATED_ROW_HEIGHT ((IS_IPAD) ? 154.0f:(IS_IPhone6Plus) ? 115.0f:(IS_IPhone6) ? 105.0f:90.0f)
#define BIO_TEXTVIEW_WIDTH ((IS_IPAD) ? 280.0f:(IS_IPhone6Plus) ? 258.0f:(IS_IPhone6) ? 231.0f:191.0f)
#define BIO_DESCRIPTION_FONT ((IS_IPAD) ? 17.0f:(IS_IPhone6Plus) ? 14.0f:(IS_IPhone6) ? 13.0f:11.0f)
#define DEFAULT_TEXTVIEW_HEIGHT ((IS_IPAD) ? 60.0f:(IS_IPhone6Plus) ? 55.0f:(IS_IPhone6) ? 50.0f:45.0f)
#define TEXT @"I am Sarah from Madrid,Spain.I can sing well and love football.Support Atletico Madrid 😊😊."

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getVideos];
    [self setupTableViewRefresh:self.profileTableView];
}

-(void)getVideos{
//    [self showAlertViewForWaitingWithTitle:@"Loading..." andSubTitle:nil];
    [self showLoading];
    [[KSUserAPI sharedUserAPI]getVideoForLastID:@"20" andRecordsPerPage:@"20" success:^(NSMutableArray *array) {
        _videoArray = array;
        _splittedvideoArray = [KSCommonUtils splitArray:_videoArray componentsPerSegment:3];
        [self.profileTableView reloadData];
        [self stopLoading];
    } failure:^(NSError *error) {
        [self stopLoading];
        [self showAlertViewWithTitle:nil andSubTitle:@"Network Not Found" andCloseButtonTitle:@"Ok" forType:alert_Error];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

#pragma mark TabelView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            CGFloat rowHeight   = [self getTableCellHeightForRowAtIndexPath:indexPath];
            return rowHeight;
        }else{
            return 50.0;
        }
    }else{
        return (self.view.frame.size.width/3)-2.0;
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    Last row should show only one row with Logout option
    return ((section == 0) && _videoArray)?2:_splittedvideoArray.count;
}

#pragma mark TabelView DataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *profileCellHeaderIdentifier = @"profileHeader";
    static NSString *profileCellthumbIdentifier = @"profileThumb1";
    if (indexPath.section == 0) {
        KSProfileTableHeader *header = [_profileTableView dequeueReusableCellWithIdentifier:profileCellHeaderIdentifier];
        if (!header) {
            NSArray *nibArr = [[NSBundle mainBundle]loadNibNamed:@"KSProfileTableHeader" owner:self options:nil];
            if (indexPath.row == 0) {
                header = [nibArr objectAtIndex:1];
            }else{
                header = [nibArr objectAtIndex:2];
            }
            KSVideo *video = [[_splittedvideoArray objectAtIndex:0] objectAtIndex:0];
            if (self.selectedUser) {
                [header loadViewWithData:self.selectedUser];
            }else {
                [header loadViewWithData:video.user];
            }
            [header setUpanimationForButtons];
            [header.bioTextView setText:TEXT];
            [header setTextViewHeight];
        }
        header.selectionStyle=UITableViewCellSelectionStyleNone;
        [header updateConstraintsIfNeeded];
        return header;
    }else{
        KSProfileThumbCell *thumbCell = [_profileTableView dequeueReusableCellWithIdentifier:profileCellthumbIdentifier];
        if (!thumbCell) {
            NSArray *nibArr = [[NSBundle mainBundle]loadNibNamed:@"KSProfileThumbCell" owner:self options:nil];
            thumbCell = [nibArr objectAtIndex:0];
            [self setupMediaWithCell:thumbCell andWithIndexPath:indexPath];
        }
        thumbCell.selectionStyle=UITableViewCellSelectionStyleNone;
        return thumbCell;
    }
}

//fill imageView with media images/video
-(void)setupMediaWithCell:(KSProfileThumbCell*)cell andWithIndexPath:(NSIndexPath*)indexPath{
    cell.cellIndexPath=indexPath;
    cell.delegate_video=(id)self;
    NSArray *filterArray = [_splittedvideoArray objectAtIndex:indexPath.row];
    for (int i=0; i<filterArray.count; i++) {
        if ([[filterArray objectAtIndex:i] isKindOfClass:[KSVideo class]]) {
            KSVideo *vedio = [filterArray objectAtIndex:i];
            UIImageView *videoButton = (UIImageView *)[cell.contentView viewWithTag:(i+1)];
            if((vedio.videoThumbPath != NULL) && ([videoButton isKindOfClass:[UIImageView class]])){
                [videoButton setContentMode: UIViewContentModeScaleAspectFill];
                [videoButton setImageWithURL:[NSURL URLWithString:vedio.videoThumbPath] placeholderImage:VIDEO_PLACEHOLDER];
                videoButton.layer.masksToBounds = YES;
            }
        }
    }
}

-(void)videoViewTappedWithIndex:(int)index andIndexPath:(NSIndexPath*)indexPath{
    
}

#pragma mark SegmentButtonDelegate method
- (void)segmentButtonTappedWithIndex:(int)buttonTag{
    
}

-(CGFloat)getTableCellHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
//   KSVideo *video = [[_splittedvideoArray objectAtIndex:0] objectAtIndex:0];
    CGFloat estimatedCellHeight = ESTIMATED_ROW_HEIGHT;
    CGFloat textViewHeight = [self textViewHeightForText:TEXT andWidth:BIO_TEXTVIEW_WIDTH];
    CGFloat cellHeight = (textViewHeight < DEFAULT_TEXTVIEW_HEIGHT)? estimatedCellHeight+DEFAULT_TEXTVIEW_HEIGHT:estimatedCellHeight+textViewHeight;
    return cellHeight;
}

- (CGFloat)textViewHeightForText:(NSString*)text andWidth:(CGFloat)width {
    UITextView *calculationView = [[UITextView alloc] init];
    [calculationView setText:text];
    [calculationView setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:BIO_DESCRIPTION_FONT]];
    [calculationView setTextColor:[UIColor colorWithRed:88.0/255.0 green:90.0/255.0 blue:90.0/255.0 alpha:1.0]];
    [calculationView sizeToFit];
    CGSize newSize = [calculationView sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    CGRect newFrame = calculationView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, width),newSize.height);
    return   newFrame.size.height;
}

@end
