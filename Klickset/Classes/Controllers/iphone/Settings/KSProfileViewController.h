//
//  KSProfileViewController.h
//  Klickset
//
//  Created by Pramuka Dias on 6/18/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSAbstractUIViewController.h"
#import "KSUser.h"

@interface KSProfileViewController : KSAbstractUIViewController
//Variables
@property (strong, nonatomic) KSUser *selectedUser;
@end
