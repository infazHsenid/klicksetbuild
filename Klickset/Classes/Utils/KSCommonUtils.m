//
//  KSCommonUtils.m
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSCommonUtils.h"
#import "KSAppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@implementation KSCommonUtils

+ (NSMutableArray *)splitArray:(NSMutableArray *)targetArray componentsPerSegment:(NSUInteger)componentsCount {
    NSMutableArray *splitedArray = [NSMutableArray array];
    NSUInteger targetArrayCount = [targetArray count];
    if (targetArrayCount > 0) {
        NSInteger index = 0;
        while (index < targetArrayCount) {
            NSInteger length = MIN(targetArrayCount - index, componentsCount);
            NSArray *subArray = [targetArray subarrayWithRange:NSMakeRange(index, length)];
            NSMutableArray *subArrayM = [NSMutableArray arrayWithArray:subArray];
            [splitedArray addObject:subArrayM];
            index = index+length;
        }
    }  return splitedArray;
}

+ (NSString *)setTextValidationWithDash:(NSString*)labelText{
    if ([labelText isEqualToString:@""] || labelText == nil) {
        return @"---";
    }
    return labelText;
}

+ (NSString *)setTextValidationWithZero:(NSString*)labelText{
    if ([labelText isEqualToString:@""] || labelText == nil) {
        return @"0";
    }
    return labelText;
}

+ (CGFloat)textViewHeightForText:(NSString *)text withFontName:(NSString *)fontName fontSize:(CGFloat)fontSize andWidth:(CGFloat)width{
    UITextView *textView = [[UITextView alloc] init];
    [textView setText:text];
    [textView setFont:[UIFont fontWithName:fontName size:fontSize]];
    [textView sizeToFit];
    CGSize newSize = [textView sizeThatFits:CGSizeMake(width, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, width),newSize.height);
    return   newFrame.size.height;
}

+ (void)playSoundwhenDroppedWithSoundFileName:(NSString *)fileName andWithExtension:(NSString *)extention{
    NSString *soundPath=[[NSBundle mainBundle] pathForResource:fileName ofType:extention];
    SystemSoundID sound;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:soundPath],&sound);
    AudioServicesPlaySystemSound(sound);
}

+ (NSAttributedString *)getAttributedText:(NSString *)text withFontName:(NSString *)fontName withFontSize:(CGFloat)fontSize textColor:(UIColor *)textColor{
    NSDictionary *attributesDict=@{NSFontAttributeName:[UIFont fontWithName:fontName size:fontSize],NSForegroundColorAttributeName:textColor};
    NSMutableAttributedString *attributeString=[[NSMutableAttributedString alloc] initWithString:text attributes:attributesDict];
    return attributeString;
}

+ (UIImage *)generateThumbImage:(NSString *)filepath
{
    NSURL *url = [NSURL fileURLWithPath:filepath];
//    AVAsset *asset = [AVAsset assetWithURL:url];
//    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//    CMTime time = [asset duration];
//    time.value = 0;
//    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
//    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
//    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
//    return thumbnail;
    
    
//    AVAsset *asset = [AVAsset assetWithURL:url];
//    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//    CMTime time = CMTimeMake(1, 1);
//    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
//    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
//    CGImageRelease(imageRef);
//    return thumbnail;
    
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *error = NULL;
    CMTime time = CMTimeMake(1, 1);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    NSLog(@"error==%@, Refimage==%@", error, refImg);
//    CGImageRelease(refImg);

    UIImage *FrameImage= [[UIImage alloc] initWithCGImage:refImg];
    return FrameImage;
}

+ (BOOL)isNilOrEmptyString:(NSString *)aString {
    return (aString == nil || aString == (id)[NSNull null] ||  aString.length == 0);
}

+ (int)getTheUniqueIDforVideoUpload{
    int uniqueId = [[NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970] * 1000] intValue];
    return uniqueId;
}

@end
