//
//  KSCommonUtils.h
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface KSCommonUtils : NSObject
/**
 Split array into arrays
 @param targetArray array that should be spliited
 @param componentsCount number of object should in an index of array
 @return splitted array
 */
+ (NSMutableArray *)splitArray:(NSMutableArray *)targetArray componentsPerSegment:(NSUInteger)componentsCount;
/**
 If string is null or empty return dash (---)
 @param labelText text on the lable
 @return text on the label or dash (---)
 **/
+ (NSString *)setTextValidationWithDash:(NSString*)labelText;
/**
 If string is null or empty return 0
 @param labelText text on the lable
 @return text on the label or 0
 **/
+ (NSString *)setTextValidationWithZero:(NSString*)labelText;
/**
 Play sound
 @param fileName name of the sound file
 @param extention file extension of the sound file
 **/
+ (void)playSoundwhenDroppedWithSoundFileName:(NSString *)fileName andWithExtension:(NSString *)extention;
/**
 Change the part of the text to Bold
 @return NSAttributedString with Bold letters
 **/
+ (NSAttributedString *)getAttributedText:(NSString *)text withFontName:(NSString *)fontName withFontSize:(CGFloat)fontSize textColor:(UIColor *)textColor;
/**
 Generate the thumb from video URL
 @param filepath path of the video file to get the video thumb image
 **/
+ (UIImage *)generateThumbImage:(NSString *)filepath;
/**
 Check the string wether its nil or empty
 @param aString passing string to be validated
 **/
+ (BOOL)isNilOrEmptyString:(NSString *)aString;
/**
 Get unique id for video when upload
 @return unique id for the video to be uploaded
 **/
+ (int)getTheUniqueIDforVideoUpload;
@end
