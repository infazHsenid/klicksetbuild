//
//  KSTextConstants.h
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#ifndef Klickset_KSTextConstants_h
#define Klickset_KSTextConstants_h

#define STORED_USER_ID @"STORED_USER_ID"

//Fonts
#define Gotham_Black @"Gotham-Black"
#define Gotham_Bold @"Gotham-Bold"
#define Gotham_Book @"Gotham-Book"
#define Gotham_BookItalic @"Gotham-BookItalic"
#define Gotham_Light @"Gotham-Light"
#define Gotham_Medium @"Gotham-Medium"

#define KFONT_Black @"Gotham-Black"
#define KFONT_Bold @"Gotham-Bold"
#define KFONT_Book @"Gotham-Book"
#define KFONT_BookItalic @"Gotham-BookItalic"
#define KFONT_Light @"Gotham-Light"
#define KFONT_Medium @"Gotham-Medium"
#define KFONT_NEW @"Helvetica"
// Helvetica Neue Fonts
#define HELVETICANEUE @"HelveticaNeue"
#define HELVETICANEUE_LIGHT @"HelveticaNeue-Light"
#define HELVETICANEUE_BOLD @"HelveticaNeue-Bold"
//Colours
#define RED_THEME_COLOR [UIColor colorWithRed:212.0/255.0 green:25.0/255.0 blue:43.0/255.0 alpha:1.0]
#define RED_SEGEMENT_TINT_COLOR [UIColor colorWithRed:140.0/255.0 green:25.0/255.0 blue:43.0/255.0 alpha:1.0]

#endif
