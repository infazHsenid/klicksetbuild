//
//  KSUserAPIConstants.h
//  Klickset
//
//  Created by Infaz Ariff on 6/5/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#ifndef Klickset_KSUserAPIConstants_h
#define Klickset_KSUserAPIConstants_h

//#define KlickSetBaseURL  @"http://www.klickset.com/klickset/web-services/build_1.5/"
#define KlickSetBaseURL  @"http://webservices.klickset.com/klickset/web-services/"
#define nexmoBaseURL  @"https://api.nexmo.com/"

//ERROR DOMAIN AND CODES
#define KS_NETWOTK_NOT_REACHABLE_ERROR @"KS_NETWOTK_NOT_REACHABLE_ERROR"
#define KS_NETWOTK_NOT_REACHABLE_ERROR_CODE 100
#define KS_PRINTABLE_ERROR_DESC @"NSLocalizedDescription"

//Images
#define USER_PLACEHOLDER [UIImage imageNamed:@"user_placeholder"]
#define VIDEO_PLACEHOLDER [UIImage imageNamed:@"vid-placeholder"]
#endif
