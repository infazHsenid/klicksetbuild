//
//  KSGlobalConstants.h
//  Klickset
//
//  Created by Infaz Ariff on 6/12/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#ifndef Klickset_KSGlobalConstants_h
#define Klickset_KSGlobalConstants_h

#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPOD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPod touch" ] )
#define IS_IPAD   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPhone4  ([[UIScreen mainScreen] bounds].size.height == 480)
#define IS_IPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE
#define IS_IPhone6  ([[UIScreen mainScreen] bounds].size.height == 667)?TRUE:FALSE
#define IS_IPhone6Plus  ([[UIScreen mainScreen] bounds].size.height == 736)?TRUE:FALSE
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define DEVICE_WIDTH [UIScreen mainScreen].bounds.size.width

#endif
