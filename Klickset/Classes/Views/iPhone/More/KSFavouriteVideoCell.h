//
//  KSFavouriteVideoCell.h
//  Klickset
//
//  Created by Infaz Ariff on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSFavouriteVideoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *userProfileButton;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *videoCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *uploadedUserLabel;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UILabel *numberOfViewsLabel;
//Autolayout constraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewLabelWidth;
@end
