//
//  KSFavouriteVideoCell.m
//  Klickset
//
//  Created by Infaz Ariff on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSFavouriteVideoCell.h"

@implementation KSFavouriteVideoCell

- (void)awakeFromNib {
    // Initialization code
    [self changeLabelsFontSize];
    _uploadedUserLabel.userInteractionEnabled=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)changeLabelsFontSize{
    CGFloat fontSize;
    if (_videoCaptionLabel) {
        fontSize = ((IS_IPAD) ? 18.0f:(IS_IPhone6Plus) ? 15.0f:(IS_IPhone6) ? 14.0f:12.0f);
        [_videoCaptionLabel setFont:[UIFont fontWithName:KFONT_NEW size:fontSize]];
        fontSize = ((IS_IPAD) ? 15.0f:(IS_IPhone6Plus) ? 11.0f:(IS_IPhone6) ? 10.0f:9.0f);
        [_uploadedUserLabel setFont:[UIFont fontWithName:KFONT_NEW size:fontSize]];
    }
    if (_headerLabel) {
        fontSize = ((IS_IPAD) ? 28.0f:(IS_IPhone6Plus) ? 26.0f:(IS_IPhone6) ? 24.0f:22.0f);
        [_headerLabel setFont:[UIFont fontWithName:KFONT_NEW size:fontSize]];
    }
    if (_numberOfViewsLabel) {
        fontSize = ((IS_IPAD) ? 15.0f:(IS_IPhone6Plus) ? 13.0f:(IS_IPhone6) ? 12.0f:11.0f);
        [_numberOfViewsLabel setFont:[UIFont fontWithName:KFONT_NEW size:fontSize]];
    }
}

- (IBAction)moreButtonTapped:(id)sender {
    
}

@end
