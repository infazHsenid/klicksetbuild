//
//  KSHomeVideoTableCell.m
//  Klickset
//
//  Created by Pramuka Dias on 6/8/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSHomeVideoTableCell.h"
#import "KSLoadingIndicator.h"
#define USER_INFO_VIEW_HEIGHT ((IS_IPAD) ? 90.0f:(IS_IPhone6Plus) ? 80.0f:(IS_IPhone6) ? 70.0f:60.0f)
#define VIDEO_PLAYER_VIEW_HEIGHT ((IS_IPAD) ? 400.0f:(IS_IPhone6Plus) ? 355.0f:(IS_IPhone6) ? 325.0f:273.0f)
#define USER_NAME_LABEL_TOP_SPACE ((IS_IPAD) ? 20.0f:(IS_IPhone6Plus) ? 13.0f:(IS_IPhone6) ? 9.0f:7.0f)
#define BOTTOM_VIEW_HEIGHT ((IS_IPAD) ? 43.0f:(IS_IPhone6Plus) ? 35.0f:(IS_IPhone6) ? 34.0f:29.0f)
#define BOTTOM_VIEW_BUTTON_TOP_SPACE ((IS_IPAD) ? 10.0f:(IS_IPhone6Plus) ? 4.0f:(IS_IPhone6) ? 4.0f:3.0f)
#define BOTTOM_VIEW_BUTTON_SIZE ((IS_IPAD) ? 26.0f:(IS_IPhone6Plus) ? 28.0f:(IS_IPhone6) ? 27.0f:23.0f)
#define USER_NAME_LABEL_FONT ((IS_IPAD) ? 23.0f:(IS_IPhone6Plus) ? 21.0f:(IS_IPhone6) ? 20.0f:18.0f)
#define VIDEO_DESCRIPTION_FONT ((IS_IPAD) ? 17.0f:(IS_IPhone6Plus) ? 14.0f:(IS_IPhone6) ? 13.0f:11.0f)
#define LABELS_FONT ((IS_IPAD) ? 16.0f:(IS_IPhone6Plus) ? 13.0f:(IS_IPhone6) ? 12.0f:10.0f)
#define FOLLOW_BUTTON_HEIGHT ((IS_IPAD) ? 22.0f:(IS_IPhone6Plus) ? 23.0f:(IS_IPhone6) ? 21.0f:19.0f)
#define LIKE_COMMENT_VIEW_HEIGHT ((IS_IPAD) ? 22.0f:(IS_IPhone6Plus) ? 29.0f:(IS_IPhone6) ? 27.0f:23.0f)
@implementation KSHomeVideoTableCell
int numberOfButtonsInBottomView = 5;
int numberOfSpacesInBottomView = 6;
int profileImageTopBottomSpace = 6;
- (void)awakeFromNib {
    // Initialization code
    [self setFontSizeForUIElements];
    [self setAutolaoutForUIElements];
    [self createCircularProfileImageView];
    _likesLabelWidth.constant =50;
}


- (void)setUpanimationForButtons{
    [_followButton setup];
    [_likeButton setup];
    [_favouriteButton setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

// create a circular profile image view
-(void)createCircularProfileImageView{
    _profilePictureImageView.layer.cornerRadius = (USER_INFO_VIEW_HEIGHT-profileImageTopBottomSpace)/2;
    _profilePictureImageView.clipsToBounds = YES;
}

-(void)setFontSizeForUIElements{
    [_userNameLabel setFont:[UIFont fontWithName:KFONT_NEW size:USER_NAME_LABEL_FONT]];
    [_postedTimeLabel setFont:[UIFont fontWithName:KFONT_NEW size:LABELS_FONT]];
}

-(void)setAutolaoutForUIElements{
    //    user info view
    _userInforViewHeight.constant = USER_INFO_VIEW_HEIGHT;
    _userNameLabelTopSpace.constant = USER_NAME_LABEL_TOP_SPACE;
    //    video view height
    _videoPlayerViewHeight.constant = VIDEO_PLAYER_VIEW_HEIGHT;
    //buttons view height
    _bottomViewHeight.constant = BOTTOM_VIEW_HEIGHT;
    //    bottom view button top space
    for (NSLayoutConstraint *constarint in _bottomViewButtonsTopHeight) {
        constarint.constant = BOTTOM_VIEW_BUTTON_TOP_SPACE;
    }
    //    bottom view buttons size
    for (NSLayoutConstraint *constarint in _bottomViewButtonSize) {
        constarint.constant = BOTTOM_VIEW_BUTTON_SIZE;
    }
    //    bottom view buttons
    for (NSLayoutConstraint *leadingConstraint in _bottomViewButtonsLeadingSpace) {
        leadingConstraint.constant = (DEVICE_WIDTH - (BOTTOM_VIEW_BUTTON_SIZE*numberOfButtonsInBottomView))/numberOfSpacesInBottomView;
    }
//    set video player view frame
    self.videoPlayerView.frame = CGRectMake(self.videoPlayerView.frame.origin.x, self.videoPlayerView.frame.origin.y, DEVICE_WIDTH, VIDEO_PLAYER_VIEW_HEIGHT);
//    follow button
    _folowButtonHeight.constant=FOLLOW_BUTTON_HEIGHT;
//    likes,comment and views height
    _likesAndCommentViewHeight.constant=LIKE_COMMENT_VIEW_HEIGHT;
}

//change text view height according to text
-(void)setTextViewHeight{
    [_videoDescriptionTextView setFont:[UIFont fontWithName:KFONT_NEW size:VIDEO_DESCRIPTION_FONT]];
    [_videoDescriptionTextView setTextColor:[UIColor blackColor]];
    [_videoDescriptionTextView sizeToFit];
    CGSize newSize = [_videoDescriptionTextView sizeThatFits:CGSizeMake(DEVICE_WIDTH, MAXFLOAT)];
    CGRect newFrame = _videoDescriptionTextView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, DEVICE_WIDTH),newSize.height);
    _videoDescriptionTextView.frame = newFrame;
    _videoDescriptionViewHeight.constant =  newFrame.size.height;
}

-(void)setValuesForLikesLabel:(NSString *)likes CommentsLabel:(NSString *)comments ViewsLabel:(NSString *)views{
    [self setLabelWithText:[self getLikesCommentsTextWithNumber:likes withLabelName:@"Likes"] labelName:_numberOfLikesLabel constraint:_likesLabelWidth];
    [self setLabelWithText:[self getLikesCommentsTextWithNumber:[NSString stringWithFormat:@"  %@",comments] withLabelName:@"Comments"] labelName:_numberOfCommentLabel constraint:_commentsLabelWidth];
    [self setLabelWithText:[self getLikesCommentsTextWithNumber:[NSString stringWithFormat:@"  %@",views] withLabelName:@"Views"] labelName:_numberOfViewsLabel constraint:_viewLabelWidth];
}

-(NSString *)getLikesCommentsTextWithNumber:(NSString *)count withLabelName:(NSString *)labelName{
    return [NSString stringWithFormat:@" %@ %@  ",count,labelName];
}

-(void)setLabelWithText:(NSString *)text labelName:(UILabel *)label constraint:(NSLayoutConstraint *)constraint{
    CGSize size = [text sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:KFONT_NEW size:LABELS_FONT]}];
    [label setText:text];
    [label setFont:[UIFont fontWithName:KFONT_NEW size:LABELS_FONT]];
    constraint.constant = size.width;
}

- (IBAction)navigateToUserProfile:(id)sender {
    [_videoCellDelegate userProfileImageTappedWithUser:_user];
}

- (IBAction)buttonActions:(id)sender {
    UIButton *button = (UIButton*)sender;
    if ((button == _favouriteButton) || (button == _likeButton) || (button == _followButton)) {
        [KSCommonUtils playSoundwhenDroppedWithSoundFileName:@"Blop" andWithExtension:@"mp3"];
        if (!button.isSelected) {
            [button setSelected:YES];
        }else{
            [button setSelected:NO];
        }
    }
}

- (IBAction)tapOnViewPlayer:(id)sender {
    if (self.avPlayer.rate > 0 && !self.avPlayer.error) {
        [self.avPlayer pause];
    }else{
        [self.avPlayer play];
    }
//    [self.videoCellDelegate videoPlayerTappedWithIndexPath:self.indexPath];
}

- (void)volumeButtonTappedWithIndexPath:(id)sender{
    if (self.avPlayer.volume == 0.0) {
        [self.avPlayer setVolume:1.0];
        self.volumeButton.selected = NO;
    }else{
        self.volumeButton.selected = YES;
        [self.avPlayer setVolume:0.0];
    }
}

//AVPlayer Notifications
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *item = [notification object];
    [item seekToTime:kCMTimeZero];
    [self.avPlayer play];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if (object == self.avPlayer && [keyPath isEqualToString:@"status"]) {
        if (self.avPlayer.status == AVPlayerStatusReadyToPlay) {
            //DISABLE THE UIACTIVITY INDICATOR HERE
            [[KSLoadingIndicator sharedInstance]stopAnimationOfLoadingView];
        } else if (self.avPlayer.status == AVPlayerStatusFailed) {
            // something went wrong. player.error should contain some information
        }
    }
}

- (IBAction)commentButtonAction:(id)sender {
    if ([sender isKindOfClass:[UIButton class]]) {
        [self navigateToCommentViewFromButton:YES];
    }else{
        [self navigateToCommentViewFromButton:NO];
    }
}

-(void)navigateToCommentViewFromButton:(BOOL)isFromButton {
    [_videoCellDelegate commentTappedAction:nil isFromButton:isFromButton];
}

- (IBAction)shareButtonAction:(id)sender {
    [_videoCellDelegate shareButtonTappedWithIdexPath:self.indexPath];
}

@end
