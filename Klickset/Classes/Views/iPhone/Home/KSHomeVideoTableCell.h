//
//  KSHomeVideoTableCell.h
//  Klickset
//
//  Created by Pramuka Dias on 6/8/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GUIPlayerView.h"
#import "KSUser.h"
#import "FlatButton.h"

@protocol HomeVideoCellDelegate <NSObject>
- (void)videoPlayerTappedWithIndexPath:(NSIndexPath*)indexPath;
- (void)userProfileImageTappedWithUser:(KSUser *)user;
- (void)commentTappedAction:(KSUser *)user isFromButton:(BOOL)isFromButton;
- (void)shareButtonTappedWithIdexPath:(NSIndexPath *)indexPath;
@end
@interface KSHomeVideoTableCell : UITableViewCell
//IBOutlets
@property (weak, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *postedTimeLabel;
@property (weak, nonatomic) IBOutlet UITextView *videoDescriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfLikesLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfViewsLabel;
@property (weak, nonatomic) IBOutlet FlatButton *followButton;
//@property (strong, nonatomic) IBOutlet GUIPlayerView *videoPlayerView;
@property (strong, nonatomic) IBOutlet UIView *videoPlayerView;
//Autolayout Constraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userInforViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoPlayerViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoDescriptionViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likesAndCommentViewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHeight;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *bottomViewButtonsLeadingSpace;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *userNameLabelTopSpace;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *bottomViewButtonsTopHeight;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *bottomViewButtonSize;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likesLabelWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentsLabelWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewLabelWidth;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

@property (weak, nonatomic) id <HomeVideoCellDelegate> videoCellDelegate;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) AVPlayer *avPlayer;
@property (strong, nonatomic) AVPlayerLayer *avPlayerLayer;
@property (strong, nonatomic) AVPlayerItem *playerItem;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *folowButtonHeight;
@property (strong, nonatomic) FlatButton *volumeButton;
@property (weak, nonatomic) IBOutlet FlatButton *likeButton;
@property (weak, nonatomic) IBOutlet FlatButton *favouriteButton;
@property (strong, nonatomic) KSUser *user;
//Methods

// Set video description text view height according to the text
-(void)setTextViewHeight;
// set values for comments , likes ,views labels
-(void)setValuesForLikesLabel:(NSString *)likes CommentsLabel:(NSString *)comments ViewsLabel:(NSString *)views;
//button Actions
- (IBAction)navigateToUserProfile:(id)sender;
// to show button selected or not
- (IBAction)buttonActions:(id)sender;
- (void)setUpanimationForButtons;
- (void)volumeButtonTappedWithIndexPath:(id)sender;
//AVPlayer Notifications
- (void)playerItemDidReachEnd:(NSNotification *)notification;
@end
