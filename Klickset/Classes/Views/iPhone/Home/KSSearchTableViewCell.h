//
//  KSSearchTableViewCell.h
//  Klickset
//
//  Created by Pramuka Dias on 7/13/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSSearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *trendingTagsValuesButton;
@property (weak, nonatomic) IBOutlet UIButton *popularTagsValuesButton;
@property (weak, nonatomic) IBOutlet UILabel *trendingTagLabel;
@property (weak, nonatomic) IBOutlet UILabel *popularTagLabel;
@end
