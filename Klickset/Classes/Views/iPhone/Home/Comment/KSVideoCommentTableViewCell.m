//
//  KSVideoCommentTableViewCell.m
//  Klickset
//
//  Created by Pramuka Dias on 7/6/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSVideoCommentTableViewCell.h"

@implementation KSVideoCommentTableViewCell
#define USER_NAME_LABEL_FONT_SIZE ((IS_IPAD) ? 23.0f:(IS_IPhone6Plus) ? 18.0f:(IS_IPhone6) ? 17.0f:16.0f)
#define COMMENT_FONT_SIZE ((IS_IPAD) ? 17.0f:(IS_IPhone6Plus) ? 15.0f:(IS_IPhone6) ? 14.0f:13.0f)
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        [self configureUIElements];
    }
    return self;
}

- (void)configureUIElements{
    [self.contentView addSubview:self.thumbnailView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.bodyLabel];
    [self.contentView addSubview:self.attachmentView];
    NSDictionary *views = @{@"thumbnailView": self.thumbnailView,
                            @"titleLabel": self.titleLabel,
                            @"bodyLabel": self.bodyLabel,
                            @"attachmentView": self.attachmentView,
                            };
    NSDictionary *metrics = @{@"tumbSize": @(AVATAR_HEIGHT),
                              @"padding": @15,
                              @"right": @10,
                              @"left": @5,
                              @"attchSize": @80,
                              };
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-left-[thumbnailView(tumbSize)]-right-[titleLabel(>=0)]-right-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-left-[thumbnailView(tumbSize)]-right-[bodyLabel(>=0)]-right-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-left-[thumbnailView(tumbSize)]-right-[attachmentView]-right-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-right-[thumbnailView(tumbSize)]-(>=0)-|" options:0 metrics:metrics views:views]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-padding-[titleLabel]-left-[bodyLabel(>=0)]-left-[attachmentView(>=0,<=attchSize)]-right-|" options:0 metrics:metrics views:views]];
}

- (void)prepareForReuse{
    [super prepareForReuse];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.titleLabel.font = [UIFont fontWithName:KFONT_NEW size:USER_NAME_LABEL_FONT_SIZE];
    self.bodyLabel.font = [UIFont fontWithName:KFONT_NEW size:COMMENT_FONT_SIZE];
    self.attachmentView.image = nil;
}

#pragma mark - Getters

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _titleLabel.userInteractionEnabled=YES;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont fontWithName:KFONT_NEW size:USER_NAME_LABEL_FONT_SIZE];
        _titleLabel.textColor = [UIColor colorWithRed:110.0/255.0 green:116.0/255.0 blue:122.0/255.0 alpha:1.0];
    }
    return _titleLabel;
}

- (UILabel *)bodyLabel{
    if (!_bodyLabel) {
        _bodyLabel = [UILabel new];
        _bodyLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _bodyLabel.backgroundColor = [UIColor clearColor];
        _bodyLabel.numberOfLines=0;
        _bodyLabel.font = [UIFont fontWithName:KFONT_NEW size:COMMENT_FONT_SIZE];
        _bodyLabel.textColor = [UIColor darkGrayColor];
    }
    return _bodyLabel;
}

- (UIImageView *)thumbnailView{
    if (!_thumbnailView) {
        _thumbnailView = [UIImageView new];
        _thumbnailView.translatesAutoresizingMaskIntoConstraints = NO;
        _thumbnailView.userInteractionEnabled=YES;
        _thumbnailView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
        _thumbnailView.layer.cornerRadius = AVATAR_HEIGHT/2.0;
        _thumbnailView.layer.masksToBounds = YES;
    }
    return _thumbnailView;
}

- (UIImageView *)attachmentView{
    if (!_attachmentView) {
        _attachmentView = [UIImageView new];
        _attachmentView.translatesAutoresizingMaskIntoConstraints = NO;
        _attachmentView.backgroundColor = [UIColor clearColor];
        _attachmentView.contentMode = UIViewContentModeScaleAspectFit;
        _attachmentView.layer.cornerRadius = AVATAR_HEIGHT/4.0;
        _attachmentView.layer.masksToBounds = YES;
    }
    return _attachmentView;
}

- (BOOL)needsPlaceholder{
    return self.thumbnailView.image ? NO : YES;
}

@end
