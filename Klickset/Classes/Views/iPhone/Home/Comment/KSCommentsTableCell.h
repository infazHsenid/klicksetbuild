//
//  KSCommentsTableCell.h
//  Klickset
//
//  Created by Infaz Ariff on 7/3/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSUser.h"

@protocol CommentsTableDelegate <NSObject>
- (void)userProfileButtonTapped:(KSUser *)user;
@end
@interface KSCommentsTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *userProfileButton;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstraint;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *userProfileButtonConstraint;
//Methods
- (void)addValuesForCommentsTableCell;
@end
