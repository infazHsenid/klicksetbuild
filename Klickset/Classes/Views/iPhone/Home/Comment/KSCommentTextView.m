//
//  KSCommentTextView.m
//  Klickset
//
//  Created by Pramuka Dias on 7/6/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSCommentTextView.h"

@implementation KSCommentTextView

- (instancetype)init{
    if (self = [super init]) {
    }
    return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview{
    [super willMoveToSuperview:newSuperview];
    self.backgroundColor = [UIColor whiteColor];
    self.placeholder = @"Say something nice";
    self.placeholderColor = [UIColor lightGrayColor];
    self.pastableMediaTypes = SLKPastableMediaTypeAll;
    self.layer.borderColor = [UIColor colorWithRed:217.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0].CGColor;
    self.layer.shouldRasterize = YES;
    self.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

@end
