//
//  KSVideoCommentTableViewCell.h
//  Klickset
//
//  Created by Pramuka Dias on 7/6/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
static CGFloat kMessageTableViewCellMinimumHeight = 50.0;
#define AVATAR_HEIGHT ((IS_IPAD) ? 50.0f:(IS_IPhone6Plus) ? 44.0f:(IS_IPhone6) ? 40.0f:36.0f)
@interface KSVideoCommentTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *bodyLabel;
@property (nonatomic, strong) UIImageView *thumbnailView;
@property (nonatomic, strong) UIImageView *attachmentView;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, readonly) BOOL needsPlaceholder;
@property (nonatomic) BOOL usedForMessage;
@end
