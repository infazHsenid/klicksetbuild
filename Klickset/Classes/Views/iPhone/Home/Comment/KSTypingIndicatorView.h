//
//  KSTypingIndicatorView.h
//  Klickset
//
//  Created by Pramuka Dias on 7/6/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SLKTypingIndicatorProtocol.h"
static CGFloat kTypingIndicatorViewMinimumHeight = 80.0;
static CGFloat kTypingIndicatorViewAvatarHeight = 30.0;
@interface KSTypingIndicatorView : UIView <SLKTypingIndicatorProtocol>
- (void)presentIndicatorWithName:(NSString *)name image:(UIImage *)image;
- (void)dismissIndicator;
@end
