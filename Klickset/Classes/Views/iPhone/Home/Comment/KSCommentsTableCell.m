//
//  KSCommentsTableCell.m
//  Klickset
//
//  Created by Infaz Ariff on 7/3/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSCommentsTableCell.h"

#define USERBUTTON_HEIGHT ((IS_IPAD) ? 50.0f:(IS_IPhone6Plus) ? 44.0f:(IS_IPhone6) ? 40.0f:36.0f)
@implementation KSCommentsTableCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)addValuesForCommentsTableCell{
    for (NSLayoutConstraint *heightConstant in _userProfileButtonConstraint) {
        heightConstant.constant = USERBUTTON_HEIGHT;
    }
    _userProfileButton.layer.cornerRadius = USERBUTTON_HEIGHT/2;
    _userProfileButton.clipsToBounds = YES;
}

- (IBAction)userProfileButtonAction:(id)sender {

}


@end
