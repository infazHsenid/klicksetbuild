//
//  KSSoundBoardCellTableViewCell.h
//  Klickset
//
//  Created by Infaz Ariff on 7/9/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"
#import "FlatButton.h"
#import "MCSwipeTableViewCell.h"
@protocol SoundBoardCellDelegate <NSObject>
- (void)playButtonTappedWithIndexPath:(NSIndexPath*)indexPath withPlayButton:(FlatButton *)playButton;
@end
@interface KSSoundBoardCell : MCSwipeTableViewCell
@property (weak, nonatomic) IBOutlet FlatButton *likeButton;
@property (weak, nonatomic) IBOutlet MarqueeLabel *soundNameLabel;
@property (weak, nonatomic) IBOutlet FlatButton *playPauseButton;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (weak, nonatomic) id <SoundBoardCellDelegate> SoundBoardCellDelegate;
//Autolay out
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *buttonWidthAndHeight;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *buttonLeadindAndTrailingSpace;
//Button Actions
- (IBAction)playPauseButtonAction:(id)sender;
- (IBAction)likeButtonAction:(id)sender;
//set slected like button and selected play button
-(void)cellForRowAtIndexPath:(NSIndexPath *)indexPath withSelectedPlayingIndexPath:(NSIndexPath *)currentPlayingIndexPath withPlayButton:(FlatButton *)playButton;
@end
