//
//  KSSoundBoardCellTableViewCell.m
//  Klickset
//
//  Created by Infaz Ariff on 7/9/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSSoundBoardCell.h"
#define BUTTON_SIZE ((IS_IPAD) ? 32.0f:(IS_IPhone6Plus) ? 22.0f:(IS_IPhone6) ? 21.0f:18.0f)
#define BUTTON_LEADIND_TRAILING_SPACE ((IS_IPAD) ?30.0f:(IS_IPhone6Plus) ? 10.0f:(IS_IPhone6) ? 14.0f:8.0f)
#define LABEL_FONT_SIZE ((IS_IPAD) ?22.0f:(IS_IPhone6Plus) ? 18.0f:(IS_IPhone6) ? 16.0f:14.0f)
@implementation KSSoundBoardCell

- (void)awakeFromNib {
    // Initialization code
    [self configureUIElements];
    [self setAutolaoutForUIElements];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureUIElements{
    [_soundNameLabel setMarqueeType:MLContinuous];
    [_soundNameLabel setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:LABEL_FONT_SIZE]];
    [_soundNameLabel setTextColor:[UIColor blackColor]];
    [_soundNameLabel setFadeLength:10.0];
}

-(void)setAutolaoutForUIElements{
    //    buttons size
    for (NSLayoutConstraint *constarint in _buttonWidthAndHeight) {
        constarint.constant = BUTTON_SIZE;
    }
//    button leading , trailing space
    for (NSLayoutConstraint *constarint in _buttonLeadindAndTrailingSpace) {
        constarint.constant = BUTTON_LEADIND_TRAILING_SPACE;
    }
}

// play pause button action
- (IBAction)playPauseButtonAction:(id)sender {
    FlatButton *button = (FlatButton *)sender;
    [self.SoundBoardCellDelegate playButtonTappedWithIndexPath:_indexPath withPlayButton:button];
}

// like button action
- (IBAction)likeButtonAction:(id)sender {
    [self buttonSelectedOrNot:(FlatButton *)sender];
}

-(void)buttonSelectedOrNot:(FlatButton *)aButton{    
    [KSCommonUtils playSoundwhenDroppedWithSoundFileName:@"Blop" andWithExtension:@"mp3"];
    if (!aButton.isSelected) {
        [aButton setSelected:YES];
    }else{
        [aButton setSelected:NO];
    }
}

//set slected like button and selected play button
-(void)cellForRowAtIndexPath:(NSIndexPath *)indexPath withSelectedPlayingIndexPath:(NSIndexPath *)currentPlayingIndexPath withPlayButton:(FlatButton *)playButton{
    if (currentPlayingIndexPath) {
        if (indexPath.row==currentPlayingIndexPath.row) {
            if (!playButton.isSelected) {
                [_playPauseButton setSelected:YES];
            }else{
                [_playPauseButton setSelected:NO];
            }
        }else{
            [_playPauseButton setSelected:NO];
        }
    }
}

@end
