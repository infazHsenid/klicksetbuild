//
//  KSSearchTableViewCell.m
//  Klickset
//
//  Created by Pramuka Dias on 7/13/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSSearchTableViewCell.h"
#define TAG_FONT ((IS_IPAD) ? 23.0f:(IS_IPhone6Plus) ? 19.0f:(IS_IPhone6) ? 17.0f:15.0f)
@implementation KSSearchTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self configureTableHeaderViewUIElements];
    [self configureTableCellUIElements];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

-(void)setCornerRadiusWithButton:(UIButton *)button{
    button.layer.cornerRadius = 5;
    button.clipsToBounds = YES;
}

-(void)configureTableCellUIElements{
    [self setCornerRadiusWithButton:self.trendingTagsValuesButton];
    [self setCornerRadiusWithButton:self.popularTagsValuesButton];
    [self.trendingTagsValuesButton.titleLabel setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:TAG_FONT]];
    [self.popularTagsValuesButton.titleLabel setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:TAG_FONT]];
}

-(void)configureTableHeaderViewUIElements{
    [self.trendingTagLabel setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:TAG_FONT]];
    [self.popularTagLabel setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:TAG_FONT]];
}

@end
