//
//  KSProfileTableHeader.m
//  Klickset
//
//  Created by Infaz Ariff on 6/18/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSProfileTableHeader.h"
#import <UIButton+AFNetworking.h>
#import "KSCommonUtils.h"
@implementation KSProfileTableHeader
#define BUTTONS_FONT ((IS_IPAD) ? 16.0f:(IS_IPhone6Plus) ? 14.0f:(IS_IPhone6) ? 13.0f:11.0f)
#define PROFILE_BUTTON_WIDTH ((IS_IPAD) ? 223.0f:(IS_IPhone6Plus) ? 120.0f:(IS_IPhone6) ? 109.0f:93.0f)
#define BIO_DESCRIPTION_FONT ((IS_IPAD) ? 17.0f:(IS_IPhone6Plus) ? 14.0f:(IS_IPhone6) ? 13.0f:11.0f)
#define BIO_TEXTVIEW_WIDTH ((IS_IPAD) ? 280.0f:(IS_IPhone6Plus) ? 258.0f:(IS_IPhone6) ? 231.0f:191.0f)
#define DEFAULT_TEXTVIEW_HEIGHT ((IS_IPAD) ? 60.0f:(IS_IPhone6Plus) ? 55.0f:(IS_IPhone6) ? 50.0f:45.0f)

- (void)awakeFromNib {
    // Initialization code
}

- (void)setUpanimationForButtons{
    [_colloctionButton setup];
    [_listButton setup];
    [_favouritesButton setup];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)loadViewWithData:(KSUser *)userObject{
    self.profileName.text = [KSCommonUtils setTextValidationWithDash:userObject.username];
    self.countryName.text = [KSCommonUtils setTextValidationWithDash:@"Spain"];
    self.bioTextView.text = [KSCommonUtils setTextValidationWithDash:userObject.bio];
//    [self setValuesForPostButton:[KSCommonUtils setTextValidationWithZero:userObject.postsCounts] followingButton:[KSCommonUtils setTextValidationWithZero:userObject.followingcount] followersButton:[KSCommonUtils setTextValidationWithZero:userObject.followersCount]];
    [self setValuesForPostButton:@"100" followingButton:@"10" followersButton:@"121"];
    [self.profileImageButton setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:userObject.image.photo_thumb] placeholderImage:USER_PLACEHOLDER];
    _profileImageButton.layer.cornerRadius = PROFILE_BUTTON_WIDTH/2;
    _profileImageButton.clipsToBounds = YES;
}

- (IBAction)segmentButtonTapped:(id)sender {
    UIButton *segmentButton;
    for (int tag = 100; tag <102; tag++) {
        segmentButton = (UIButton *)[self viewWithTag:tag];
        if ([segmentButton isKindOfClass:[UIButton class]]) {
            segmentButton.selected = NO;
        }
    }
    segmentButton = (UIButton *)sender;
    switch (segmentButton.tag) {
        case 100:
            segmentButton.selected = YES;
            [self.buttonDelegate segmentButtonTappedWithIndex:0];
            [self changeFavouriteLabelsColorWithSelected:NO];
            break;
        case 101:
            segmentButton.selected = YES;
            [self.buttonDelegate segmentButtonTappedWithIndex:1];
            [self changeFavouriteLabelsColorWithSelected:NO];
            break;
        case 102:
            segmentButton.selected = YES;
            [self.buttonDelegate segmentButtonTappedWithIndex:2];
            [self changeFavouriteLabelsColorWithSelected:YES];
            break;
        default:
            break;
    }
}

-(void)changeFavouriteLabelsColorWithSelected:(BOOL)isSelected{
    UIColor *labelTextColor;
    if (isSelected) {
        labelTextColor = RED_THEME_COLOR;
    }else{
        labelTextColor = [UIColor darkGrayColor];
    }
    _numberOfFavourites.textColor=labelTextColor;
    _favouriteLabel.textColor=labelTextColor;
}

-(void)setValuesForPostButton:(NSString *)posts followingButton:(NSString *)followings followersButton:(NSString *)followers{
    [self setButtonWithText:[self getLikesCommentsTextWithNumber:[self keepExtraSpaceBetweenText:posts] withLabelName:[self keepExtraSpaceBetweenText:@"Posts "]] buttonName:_postButton constraint:_postButtonWidth];
    [self setButtonWithText:[self getLikesCommentsTextWithNumber:[self keepExtraSpaceBetweenText:followings] withLabelName:[self keepExtraSpaceBetweenText:@"Following "]] buttonName:_followingButton constraint:_followingButtonWidth];
    [self setButtonWithText:[self getLikesCommentsTextWithNumber:[self keepExtraSpaceBetweenText:followers] withLabelName:[self keepExtraSpaceBetweenText:@"Followers "]] buttonName:_followersButton constraint:_followersButtonWidth];
}

-(NSString *)keepExtraSpaceBetweenText:(NSString *)aString{
    NSString *whiteSpaceString =[NSString stringWithFormat:@" %@",aString];
    return whiteSpaceString;
}

-(NSAttributedString *)getLikesCommentsTextWithNumber:(NSString *)count withLabelName:(NSString *)buttonName{
    NSMutableAttributedString *countText=[[KSCommonUtils getAttributedText:count withFontName:HELVETICANEUE_BOLD withFontSize:BUTTONS_FONT textColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.7]] mutableCopy];
    NSAttributedString *buttonTitle = [KSCommonUtils getAttributedText:buttonName withFontName:KFONT_NEW withFontSize:BUTTONS_FONT textColor:[UIColor darkGrayColor]];
    [countText appendAttributedString:buttonTitle];
    return countText;
}

-(void)setButtonWithText:(NSAttributedString *)title buttonName:(UIButton *)button constraint:(NSLayoutConstraint *)constraint{
    CGSize size = [[title string] sizeWithAttributes:@{NSFontAttributeName:[UIFont fontWithName:HELVETICANEUE_BOLD size:BUTTONS_FONT]}];
    [button setAttributedTitle:title forState:UIControlStateNormal];
    [button sizeToFit];
    constraint.constant = size.width;
}

//change text view height according to text
-(void)setTextViewHeight{
    [_bioTextView setFont:[UIFont fontWithName:HELVETICANEUE_LIGHT size:BIO_DESCRIPTION_FONT]];
    [_bioTextView setTextColor:[UIColor colorWithRed:88.0/255.0 green:90.0/255.0 blue:90.0/255.0 alpha:1.0]];
    [_bioTextView sizeToFit];
    CGSize newSize = [_bioTextView sizeThatFits:CGSizeMake(BIO_TEXTVIEW_WIDTH, MAXFLOAT)];
    CGRect newFrame = _bioTextView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, BIO_TEXTVIEW_WIDTH),newSize.height);
    _bioTextView.frame = newFrame;
    _bioTextViewHeight.constant = (newFrame.size.height < DEFAULT_TEXTVIEW_HEIGHT)? DEFAULT_TEXTVIEW_HEIGHT: newFrame.size.height;
}

@end
