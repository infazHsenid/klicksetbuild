//
//  KSProfileTableHeader.h
//  Klickset
//
//  Created by Infaz Ariff on 6/18/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSVideo.h"
#import "FlatButton.h"

@protocol SegmentButtonDelegate <NSObject>
- (void)segmentButtonTappedWithIndex:(int)buttonTag;
@end
@interface KSProfileTableHeader : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *profileImageButton;
@property (weak, nonatomic) IBOutlet UILabel *profileName;
@property (weak, nonatomic) IBOutlet UILabel *countryName;
@property (weak, nonatomic) IBOutlet UITextView *bioTextView;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *followingButton;
@property (weak, nonatomic) IBOutlet UIButton *followersButton;
@property (weak, nonatomic) IBOutlet FlatButton *colloctionButton;
@property (weak, nonatomic) IBOutlet FlatButton *listButton;
@property (weak, nonatomic) IBOutlet FlatButton *favouritesButton;
@property (weak, nonatomic) IBOutlet UILabel *numberOfFavourites;
@property (weak, nonatomic) IBOutlet UILabel *favouriteLabel;
@property (weak, nonatomic) id <SegmentButtonDelegate> buttonDelegate;
//Autolayout Constarint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postButtonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followingButtonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followersButtonWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bioTextViewHeight;
//Methods
- (void)loadViewWithData:(KSUser *)userObject;
- (void)setUpanimationForButtons;
-(void)setTextViewHeight;
@end
