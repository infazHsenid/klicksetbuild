//
//  KSProfileThumbCell.m
//  Klickset
//
//  Created by Infaz Ariff on 6/18/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSProfileThumbCell.h"

@implementation KSProfileThumbCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)firstVideoViewTapped:(id)sender {
    [self.delegate_video videoViewTappedWithIndex:FIRST_VIEW andIndexPath:_cellIndexPath];
}

- (IBAction)secondVideoViewTapped:(id)sender {
    [self.delegate_video videoViewTappedWithIndex:SECOND_VIEW andIndexPath:_cellIndexPath];
}

- (IBAction)thirdVideoViewTapped:(id)sender {
    [self.delegate_video videoViewTappedWithIndex:THIRD_VIEW andIndexPath:_cellIndexPath];
}
@end
