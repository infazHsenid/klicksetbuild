//
//  KSProfileThumbCell.h
//  Klickset
//
//  Created by Infaz Ariff on 6/18/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, ButtonOrder) {
    FIRST_VIEW = 1,
    SECOND_VIEW,
    THIRD_VIEW
};
@protocol MediaButtonDelagete <NSObject>
@optional
-(void)videoViewTappedWithIndex:(int)index andIndexPath:(NSIndexPath*)indexPath;
@end
@interface KSProfileThumbCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *videoThumbButtonOne;
@property (weak, nonatomic) IBOutlet UIImageView *videoThumbButtonTwo;
@property (weak, nonatomic) IBOutlet UIImageView *videoThumbButtonThree;
@property (strong,nonatomic) NSIndexPath *cellIndexPath;
@property (nonatomic, assign) id<MediaButtonDelagete> delegate_video;
@end
