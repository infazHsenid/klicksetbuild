//
//  KSPeoplesTableViewCell.m
//  Klickset
//
//  Created by Pramuka Dias on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import "KSPeoplesTableViewCell.h"

@implementation KSPeoplesTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [self makeFriendProfileImageViewCircle];
    [self setFriendNameLabelFontSize];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// make a circular profile image view 
-(void)makeFriendProfileImageViewCircle{
    _friendProfileImage.layer.cornerRadius = _friendProfileImage.frame.size.width/2;
    _friendProfileImage.clipsToBounds = YES;
}

//change friend name label font
-(void)setFriendNameLabelFontSize{
    CGFloat fontSize = ((IS_IPAD) ? 26.0f:(IS_IPhone6Plus) ? 24.0f:(IS_IPhone6) ? 22.0f:21.0f);
    [_friendNameLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:fontSize]];
}

@end
