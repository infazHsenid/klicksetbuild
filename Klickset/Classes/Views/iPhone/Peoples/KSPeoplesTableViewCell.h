//
//  KSPeoplesTableViewCell.h
//  Klickset
//
//  Created by Pramuka Dias on 6/25/15.
//  Copyright (c) 2015 Klickset. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSPeoplesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *friendProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *friendNameLabel;
@end
